# Unexposed API
	
## Functions:

### Plugin API
- `BASS_PluginLoad` (security concern)
- `BASS_PluginFree` (security concern)
- `BASS_PluginGetInfo` (security concern)

### Sample API
- `BASS_SampleSetData` (can't retrieve BLOB)
- `BASS_SampleGetData` (can't retrieve BLOB)
- `BASS_SampleCreate` (because `BASS_SampleSetData` isn't exposed)

### Stream API
- `BASS_StreamCreate` (too low level access, can't do anything useful with a callback)
- `BASS_StreamCreateFile` (the exact function isn't exposed because it's more useful to operate with VDF archived files)
- `BASS_StreamPutData` (can't retrieve BLOB)
- `BASS_StreamPutFileData` (can't retrieve BLOB)

### Record API
- `BASS_RecordGetDeviceInfo` (privacy concern)
- `BASS_RecordInit` (privacy concern)
- `BASS_RecordSetDevice` (privacy concern)
- `BASS_RecordGetDevice` (privacy concern)
- `BASS_RecordFree` (privacy concern)
- `BASS_RecordGetInfo` (privacy concern)
- `BASS_RecordGetInputName` (privacy concern)
- `BASS_RecordSetInput` (privacy concern)
- `BASS_RecordGetInput` (privacy concern)
- `BASS_RecordStart` (privacy concern)

### Channel API
- `BASS_ChannelGetData` (too low level access)
- `BASS_ChannelSetSync` (too low level access)
- `BASS_ChannelRemoveSync` (too low level access)
- `BASS_ChannelSetDSP` (too low level access)
- `BASS_ChannelRemoveDSP` (too low level access)
- `BASS_ChannelSetAttributeEx` (i don't have an idea how to expose it)
- `BASS_ChannelGetAttributeEx` (i don't have an idea how to expose it)

### FX API
- `BASS_FXSetParameters` (i don't have an idea how to expose it)
- `BASS_FXGetParameters` (i don't have an idea how to expose it)
- `BASS_FXReset` (didn't expose it, because of two above functions also weren't exposed)

## Constants:
### Platform specific
- `BASS_CONFIG_IOS_MIXAUDIO` (different platform, not neeeded)
- `BASS_CONFIG_IOS_SPEAKER` (different platform, not neeeded)
- `BASS_CONFIG_AIRPLAY` (different platform, not needed)
- `BASS_CONFIG_IOS_NOCATEGORY` (different platform, not needed)
- `BASS_CONFIG_IOS_NOCATEGORY` (different platform, not needed)
- `BASS_CONFIG_IOS_NOTIFY` (different platform, not needed)
- `BASS_DEVICES_AIRPLAY` (different platform, not needed)
- `BASS_IOSNOTIFY_INTERRUPT` (different platform, not needed)
- `BASS_IOSNOTIFY_INTERRUPT_END` (different platform, not needed)

### Utility macro
- `BASS_SPEAKER_N` (i don't see any reason to expose this, since it's a simple bitshift operation << 24)

### Stream API
- `BASS_STREAMPROC_END` (because BASS_StreamPutData isn't exposed)
- `STREAMPROC_DUMMY` (because BASS_StreamCreate isn't exposed)
- `STREAMPROC_PUSH` (because BASS_StreamCreate isn't exposed)
- `BASS_FILEDATA_END` (because BASS_StreamPutFileData isn't exposed)

### Record API
- `BASS_CONFIG_REC_BUFFER` (because record API isn't exposed)
- `BASS_RECORD_PAUSE` (because record API isn't exposed)
- `BASS_RECORD_ECHOCANCEL` (because record API isn't exposed)
- `BASS_RECORD_AGC` (because record API isn't exposed)
- `BASS_CTYPE_RECORD` (because record API isn't exposed)
- `BASS_INPUT_OFF` (because record API isn't exposed)
- `BASS_INPUT_ON` (because record API isn't exposed)
- `BASS_INPUT_TYPE_MASK` (because record API isn't exposed)
- `BASS_INPUT_TYPE_UNDEF` (because record API isn't exposed)
- `BASS_INPUT_TYPE_DIGITAL` (because record API isn't exposed)
- `BASS_INPUT_TYPE_LINE` (because record API isn't exposed)
- `BASS_INPUT_TYPE_MIC` (because record API isn't exposed)
- `BASS_INPUT_TYPE_SYNTH` (because record API isn't exposed)
- `BASS_INPUT_TYPE_CD` (because record API isn't exposed)
- `BASS_INPUT_TYPE_PHONE` (because record API isn't exposed)
- `BASS_INPUT_TYPE_SPEAKER` (because record API isn't exposed)
- `BASS_INPUT_TYPE_WAVE` (because record API isn't exposed)
- `BASS_INPUT_TYPE_AUX` (because record API isn't exposed)
- `BASS_INPUT_TYPE_ANALOG` (because record API isn't exposed)

### Channel API
- `BASS_SYNC_POS` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_END` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_META` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_SLIDE` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_STALL` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_DOWNLOAD` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_FREE` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_SETPOS` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_MUSICPOS` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_MUSICINST` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_MUSICFX` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_OGG_CHANGE` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_MIXTIME` (because BASS_ChannelSetSync isn't exposed)
- `BASS_SYNC_ONETIME` (because BASS_ChannelSetSync isn't exposed)
- `BASS_DATA_AVAILABLE` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FIXED` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FLOAT` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FFT256` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FFT512` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FFT1024` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FFT2048` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FFT4096` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FFT8192` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FFT16384` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FFT_INDIVIDUAL` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FFT_NOWINDOW` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FFT_REMOVEDC` (because BASS_ChannelGetData isn't exposed)
- `BASS_DATA_FFT_COMPLEX` (because BASS_ChannelGetData isn't exposed)

### FX API
- `BASS_DX8_PHASE_NEG_180` (because BASS_FXSetParameters isn't exposed)
- `BASS_DX8_PHASE_NEG_90` (because BASS_FXSetParameters isn't exposed)
- `BASS_DX8_PHASE_ZERO` (because BASS_FXSetParameters isn't exposed)
- `BASS_DX8_PHASE_90` (because BASS_FXSetParameters isn't exposed)
- `BASS_DX8_PHASE_180` (because BASS_FXSetParameters isn't exposed)