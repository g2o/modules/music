# bass

## Introduction

BASS module binds a third-party BASS C Library (version 2.4.11), which provides squirrel sound management functionallity.  
It also adds a support for playing audio files from VDFS archives via `BASS_StreamCreateFile` function.  
This library isn't a complete bind of the BASS, if you're interested why certain functionallity isn't available, you can read more in [UNEXPOSED-API.md](https://gitlab.com/g2o/modules/bass/-/blob/main/UNEXPOSED_API.md?ref_type=heads).

## Requirements

This module requires [bass.dll](https://gitlab.com/GothicMultiplayerTeam/modules/bass/-/raw/main/dependencies/bass/lib/bass.dll?ref_type=heads) file to be placed into `GAME_DIR/System`, otherwise it won't work.

## Documentation

https://gothicmultiplayerteam.gitlab.io/modules/bass
