# Home

## Introduction

BASS module binds a third-party BASS C Library (version 2.4.11), which provides squirrel sound management functionallity.  
It also adds a support for playing audio files from VDFS archives via `BASS_StreamCreateFile` function.  
This library isn't a complete bind of the BASS, if you're interested why certain functionallity isn't available, you can read more in [UNEXPOSED-API.md](https://gitlab.com/g2o/modules/bass/-/blob/main/UNEXPOSED_API.md?ref_type=heads).