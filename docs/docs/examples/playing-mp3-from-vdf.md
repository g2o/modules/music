Simple code example that demonstrates how to play the mp3 file placed in `_WORK/DATA/SOUND` or `_WORK/DATA/MUSIC` path inside vdf archive.

```js
local stream = null

addEventHandler("onInit", function()
{
    // Initialize BASS
    BASS_Init(-1, 44000, 0)

    // Create the stream to a file located in vdf archive
    stream = BASS_StreamCreateFile("music.mp3", 0)

    // Play the sound and make it looping (when it's finished, it will play again)
    BASS_ChannelPlay(stream, true)
})
```