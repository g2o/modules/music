#pragma once

#include <sqapi.h>

SQInteger sq_bass_setconfigptr(HSQUIRRELVM vm);
SQInteger sq_bass_getconfigptr(HSQUIRRELVM vm);
SQInteger sq_bass_getdeviceinfo(HSQUIRRELVM vm);
SQInteger sq_bass_init(HSQUIRRELVM vm);
SQInteger sq_bass_getinfo(HSQUIRRELVM vm);
SQInteger sq_bass_get3dfactors(HSQUIRRELVM vm);
SQInteger sq_bass_set3dposition(HSQUIRRELVM vm);
SQInteger sq_bass_get3dposition(HSQUIRRELVM vm);
SQInteger sq_bass_geteaxparameters(HSQUIRRELVM vm);
SQInteger sq_bass_musicload(HSQUIRRELVM vm);
SQInteger sq_bass_sampleload(HSQUIRRELVM vm);
SQInteger sq_bass_samplegetinfo(HSQUIRRELVM vm);
SQInteger sq_bass_samplesetinfo(HSQUIRRELVM vm);
SQInteger sq_bass_samplegetchannels(HSQUIRRELVM vm);
SQInteger sq_bass_streamcreatefile(HSQUIRRELVM vm);
SQInteger sq_bass_streamcreateurl(HSQUIRRELVM vm);
SQInteger sq_bass_recordgetdeviceinfo(HSQUIRRELVM vm);
SQInteger sq_bass_recordgetinfo(HSQUIRRELVM vm);
SQInteger sq_bass_recordgetinput(HSQUIRRELVM vm);
SQInteger sq_bass_recordstart(HSQUIRRELVM vm);
SQInteger sq_bass_channelgetinfo(HSQUIRRELVM vm);
SQInteger sq_bass_channelgetattribute(HSQUIRRELVM vm);
SQInteger sq_bass_channelget3dattributes(HSQUIRRELVM vm);
SQInteger sq_bass_channelset3dposition(HSQUIRRELVM vm);
SQInteger sq_bass_channelget3dposition(HSQUIRRELVM vm);
SQInteger sq_bass_channelgetlevelex(HSQUIRRELVM vm);
