#include "bass_vdfs.h"
#include "gothic-api.h"

void __stdcall BASS_VDF_FileCloseProc(void* user)
{
	zFILE* file = reinterpret_cast<zFILE*>(user);

	file->Close();
	delete file;
}

QWORD __stdcall BASS_VDF_FileLenProc(void* user)
{
	zFILE* file = reinterpret_cast<zFILE*>(user);
	return file->Size();
}

DWORD __stdcall BASS_VDF_FileReadProc(void* buffer, DWORD length, void* user)
{
	zFILE* file = reinterpret_cast<zFILE*>(user);
	return file->Read(buffer, length);
}

bool __stdcall BASS_VDF_FileSeekProc(QWORD offset, void* user)
{
	zFILE* file = reinterpret_cast<zFILE*>(user);
	return file->Seek(static_cast<long>(offset)) >= 0;
}