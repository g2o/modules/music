#include "sqFunction.h"
#include "sqUtils.h"
#include "gothic-api.h"
#include "bass_vdfs.h"

#include <bass.h>
#include <vector>
#include <cctype>

Sqrat::string bass_configptr_options[2];
SQInteger sq_bass_setconfigptr(HSQUIRRELVM vm)
{
	SQInteger option;
	sq_getinteger(vm, 2, &option);

	const SQChar* value;
	sq_getstring(vm, 3, &value);

	if (option >= BASS_CONFIG_NET_AGENT && option <= BASS_CONFIG_NET_PROXY)
	{
		int idx = option - BASS_CONFIG_NET_AGENT;

		bass_configptr_options[idx] = value;
		value = bass_configptr_options[idx].c_str();
	}

	sq_pushbool(vm, BASS_SetConfigPtr(option, value));
	return 1;
}

SQInteger sq_bass_getconfigptr(HSQUIRRELVM vm)
{
	SQInteger option;
	sq_getinteger(vm, 2, &option);

	void* result = BASS_GetConfigPtr(option);
	if (result != nullptr)
		sq_pushstring(vm, reinterpret_cast<const SQChar*>(result), -1);
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_getdeviceinfo(HSQUIRRELVM vm)
{
	SQInteger device;
	sq_getinteger(vm, 2, &device);

	BASS_DEVICEINFO info;

	if (BASS_GetDeviceInfo(device, &info))
	{
		sq_newtable(vm);

		sq_newslot_string(vm, "name", info.name);
		sq_newslot_string(vm, "driver", info.driver);
		sq_newslot_integer(vm, "flags", info.flags);
	}
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_init(HSQUIRRELVM vm)
{
	SQInteger device;
	sq_getinteger(vm, 2, &device);

	SQInteger freq;
	sq_getinteger(vm, 3, &freq);

	SQInteger flags;
	sq_getinteger(vm, 4, &flags);

	sq_pushbool(vm, BASS_Init(device, freq, flags, NULL, NULL));
	return 1;
}

SQInteger sq_bass_getinfo(HSQUIRRELVM vm)
{
	BASS_INFO info;

	if (BASS_GetInfo(&info))
	{
		sq_newtable(vm);

		sq_newslot_integer(vm, "flags", info.flags);
		sq_newslot_integer(vm, "hwsize", info.hwsize);
		sq_newslot_integer(vm, "hwfree", info.hwfree);
		sq_newslot_integer(vm, "freesam", info.freesam);
		sq_newslot_integer(vm, "free3d", info.free3d);
		sq_newslot_integer(vm, "minrate", info.minrate);
		sq_newslot_integer(vm, "maxrate", info.maxrate);
		sq_newslot_bool(vm, "eax", info.eax);
		sq_newslot_integer(vm, "minbuf", info.minbuf);
		sq_newslot_integer(vm, "dsver", info.dsver);
		sq_newslot_integer(vm, "latency", info.latency);
		sq_newslot_integer(vm, "initflags", info.initflags);
		sq_newslot_integer(vm, "speakers", info.speakers);
		sq_newslot_integer(vm, "freq", info.freq);
	}
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_get3dfactors(HSQUIRRELVM vm)
{
	SQFloat distf;
	SQFloat rollf;
	SQFloat doppf;

	if (BASS_Get3DFactors(&distf, &rollf, &doppf))
	{
		sq_newtable(vm);

		sq_newslot_float(vm, "distf", distf);
		sq_newslot_float(vm, "rollf", rollf);
		sq_newslot_float(vm, "doppf", doppf);
	}
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_set3dposition(HSQUIRRELVM vm)
{
	BASS_3DVECTOR pos;
	sq_get_float(vm, 2, "x", &pos.x);
	sq_get_float(vm, 2, "y", &pos.y);
	sq_get_float(vm, 2, "z", &pos.z);

	BASS_3DVECTOR vel;
	sq_get_float(vm, 3, "x", &vel.x);
	sq_get_float(vm, 3, "y", &vel.y);
	sq_get_float(vm, 3, "z", &vel.z);

	BASS_3DVECTOR front;
	sq_get_float(vm, 4, "x", &front.x);
	sq_get_float(vm, 4, "y", &front.y);
	sq_get_float(vm, 4, "z", &front.z);

	BASS_3DVECTOR top;
	sq_get_float(vm, 5, "x", &top.x);
	sq_get_float(vm, 5, "y", &top.y);
	sq_get_float(vm, 5, "z", &top.z);

	sq_pushbool(vm, BASS_Set3DPosition(&pos, &vel, &front, &top));
	return 1;
}

SQInteger sq_bass_get3dposition(HSQUIRRELVM vm)
{
	BASS_3DVECTOR pos;
	BASS_3DVECTOR vel;
	BASS_3DVECTOR front;
	BASS_3DVECTOR top;

	if (BASS_Get3DPosition(&pos, &vel, &front, &top))
	{
		sq_newtable(vm);

		sq_pushstring(vm, "pos", -1);
		sq_pushvec3(vm, pos.x, pos.y, pos.z);
		sq_newslot(vm, -3, SQFalse);

		sq_pushstring(vm, "vel", -1);
		sq_pushvec3(vm, vel.x, vel.y, vel.z);
		sq_newslot(vm, -3, SQFalse);

		sq_pushstring(vm, "front", -1);
		sq_pushvec3(vm, front.x, front.y, front.z);
		sq_newslot(vm, -3, SQFalse);

		sq_pushstring(vm, "top", -1);
		sq_pushvec3(vm, top.x, top.y, top.z);
		sq_newslot(vm, -3, SQFalse);
	}
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_geteaxparameters(HSQUIRRELVM vm)
{
	DWORD env;
	SQFloat vol;
	SQFloat decay;
	SQFloat damp;

	if (BASS_GetEAXParameters(&env, &vol, &decay, &damp))
	{
		sq_newtable(vm);

		sq_newslot_integer(vm, "env", env);
		sq_newslot_float(vm, "vol", vol);
		sq_newslot_float(vm, "decay", decay);
		sq_newslot_float(vm, "damp", damp);
	}
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_musicload(HSQUIRRELVM vm)
{
	const SQChar* file;
	sq_getstring(vm, 2, &file);

	SQInteger offset;
	sq_getinteger(vm, 3, &offset);

	SQInteger length;
	sq_getinteger(vm, 4, &length);

	SQInteger flags;
	sq_getinteger(vm, 5, &flags);

	SQInteger freq;
	sq_getinteger(vm, 6, &freq);

	HMUSIC hmusic = BASS_MusicLoad(false, &file, offset, length, flags, freq);

	if (hmusic)
		sq_pushinteger(vm, hmusic);
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_sampleload(HSQUIRRELVM vm)
{
	const SQChar* file;
	sq_getstring(vm, 2, &file);

	SQInteger offset;
	sq_getinteger(vm, 3, &offset);

	SQInteger length;
	sq_getinteger(vm, 4, &length);

	SQInteger max;
	sq_getinteger(vm, 5, &max);

	SQInteger flags;
	sq_getinteger(vm, 6, &flags);

	HSAMPLE hsample = BASS_SampleLoad(false, &file, offset, length, max, flags);

	if (hsample)
		sq_pushinteger(vm, hsample);
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_samplegetinfo(HSQUIRRELVM vm)
{
	SQInteger hsample;
	sq_getinteger(vm, 2, &hsample);

	BASS_SAMPLE info;

	if (BASS_SampleGetInfo(hsample, &info))
		Sqrat::PushVar(vm, &info);
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_samplesetinfo(HSQUIRRELVM vm)
{
	SQInteger hsample;
	sq_getinteger(vm, 2, &hsample);

	BASS_SAMPLE* info = Sqrat::ClassType<BASS_SAMPLE>::GetInstance(vm, 3);
	if (!info)
		return sq_throwerror(vm, "invalid type, expected BassSample");

	sq_pushbool(vm, BASS_SampleSetInfo(hsample, info));
	return 1;
}

SQInteger sq_bass_samplegetchannels(HSQUIRRELVM vm)
{
	SQInteger hsample;
	sq_getinteger(vm, 2, &hsample);

	SQInteger channelsCount = BASS_SampleGetChannels(hsample, nullptr);
	if (channelsCount == -1)
	{
		sq_pushnull(vm);
		return 1;
	}

	std::vector<HCHANNEL> channels;
	channels.resize(channelsCount);

	if (BASS_SampleGetChannels(hsample, &channels[0]) != -1)
	{
		sq_newarray(vm, channelsCount);

		for (int i = 0; i < channels.size(); ++i)
		{
			sq_pushinteger(vm, i);
			sq_pushinteger(vm, channels[i]);
			sq_set(vm, -3);
		}
	}
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_streamcreatefile(HSQUIRRELVM vm)
{
	const SQChar* filename;
	sq_getstring(vm, 2, &filename);

	SQInteger flags;
	sq_getinteger(vm, 3, &flags);

	zFILE* file = zfactory->CreateZFile(filename);
	zoptions->ChangeDir(DIR_SOUND);

	if (!file->Exists())
		zoptions->ChangeDir(DIR_MUSIC);

	if (!file->Exists())
	{
		delete file;

		Sqrat::string errorMsg = "Cannot find audio file \"";
		errorMsg += filename;
		errorMsg += "\"\n";
		errorMsg += "Make sure it's placed in one of these paths:\n";
		errorMsg += "- \"_Work/Data/Music/\"\n";
		errorMsg += "- \"_Work/Data/Sound/\"\n";

		return sq_throwerror(vm, errorMsg.c_str());
	}

	if (file->Open(false))
	{
		delete file;

		return sq_throwerror(vm, "Problem with opening audio file");
	}

	BASS_FILEPROCS bass_vdf_fileprocs{ BASS_VDF_FileCloseProc, BASS_VDF_FileLenProc, BASS_VDF_FileReadProc, BASS_VDF_FileSeekProc };

	sq_pushinteger(vm, BASS_StreamCreateFileUser(STREAMFILE_BUFFER, flags, &bass_vdf_fileprocs, reinterpret_cast<void*>(file)));
	return 1;
}

SQInteger sq_bass_streamcreateurl(HSQUIRRELVM vm)
{
	const SQChar* url;
	sq_getstring(vm, 2, &url);

	SQInteger offset;
	sq_getinteger(vm, 3, &offset);

	SQInteger flags;
	sq_getinteger(vm, 4, &flags);

	sq_pushinteger(vm, BASS_StreamCreateURL(url, offset, flags, nullptr, nullptr));
	return 1;
}

SQInteger sq_bass_channelgetinfo(HSQUIRRELVM vm)
{
	SQInteger handle;
	sq_getinteger(vm, 2, &handle);

	BASS_CHANNELINFO info;

	if (BASS_ChannelGetInfo(handle, &info))
	{
		sq_newtable(vm);

		sq_newslot_integer(vm, "freq", info.freq);
		sq_newslot_integer(vm, "chans", info.chans);
		sq_newslot_integer(vm, "flags", info.flags);
		sq_newslot_integer(vm, "ctype", info.ctype);
		sq_newslot_integer(vm, "plugin", info.plugin);
		sq_newslot_integer(vm, "origres", info.origres);
		sq_newslot_integer(vm, "sample", info.sample);
		sq_newslot_string(vm, "filename", info.filename);
	}
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_channelgetattribute(HSQUIRRELVM vm)
{
	SQInteger handle;
	sq_getinteger(vm, 2, &handle);

	SQInteger attrib;
	sq_getinteger(vm, 3, &attrib);

	SQFloat value;

	if (BASS_ChannelGetAttribute(handle, attrib, &value))
		sq_pushfloat(vm, value);
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_channelget3dattributes(HSQUIRRELVM vm)
{
	SQInteger handle;
	sq_getinteger(vm, 2, &handle);

	DWORD mode;
	SQFloat min;
	SQFloat max;
	DWORD iangle;
	DWORD oangle;
	SQFloat outvol;

	if (BASS_ChannelGet3DAttributes(handle, &mode, &min, &max, &iangle, &oangle, &outvol))
	{
		sq_newtable(vm);

		sq_newslot_integer(vm, "mode", mode);
		sq_newslot_float(vm, "min", min);
		sq_newslot_float(vm, "max", max);
		sq_newslot_integer(vm, "iangle", iangle);
		sq_newslot_integer(vm, "oangle", oangle);
		sq_newslot_float(vm, "outvol", outvol);
	}
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_channelset3dposition(HSQUIRRELVM vm)
{
	SQInteger handle;
	sq_getinteger(vm, 2, &handle);

	BASS_3DVECTOR pos;
	sq_get_float(vm, 3, "x", &pos.x);
	sq_get_float(vm, 3, "y", &pos.y);
	sq_get_float(vm, 3, "z", &pos.z);

	BASS_3DVECTOR orient;
	sq_get_float(vm, 4, "x", &orient.x);
	sq_get_float(vm, 4, "y", &orient.y);
	sq_get_float(vm, 4, "z", &orient.z);

	BASS_3DVECTOR vel;
	sq_get_float(vm, 5, "x", &vel.x);
	sq_get_float(vm, 5, "y", &vel.y);
	sq_get_float(vm, 5, "z", &vel.z);

	sq_pushbool(vm, BASS_ChannelSet3DPosition(handle, &pos, &orient, &vel));
	return 1;
}

SQInteger sq_bass_channelget3dposition(HSQUIRRELVM vm)
{
	SQInteger handle;
	sq_getinteger(vm, 2, &handle);

	BASS_3DVECTOR pos;
	BASS_3DVECTOR orient;
	BASS_3DVECTOR vel;

	if (BASS_ChannelGet3DPosition(handle, &pos, &orient, &vel))
	{
		sq_newtable(vm);

		sq_pushstring(vm, "pos", -1);
		sq_pushvec3(vm, pos.x, pos.y, pos.z);
		sq_newslot(vm, -3, SQFalse);

		sq_pushstring(vm, "orient", -1);
		sq_pushvec3(vm, orient.x, orient.y, orient.z);
		sq_newslot(vm, -3, SQFalse);

		sq_pushstring(vm, "vel", -1);
		sq_pushvec3(vm, vel.x, vel.y, vel.z);
		sq_newslot(vm, -3, SQFalse);
	}
	else
		sq_pushnull(vm);

	return 1;
}

SQInteger sq_bass_channelgetlevelex(HSQUIRRELVM vm)
{
	SQInteger handle;
	sq_getinteger(vm, 2, &handle);

	SQFloat length;
	sq_getfloat(vm, 3, &length);

	SQInteger flags;
	sq_getinteger(vm, 4, &flags);

	std::vector<float> levels;

	if (!flags)
	{
		BASS_CHANNELINFO ci;

		if (BASS_ChannelGetInfo(handle, &ci))
			levels.resize(ci.chans);
	}
	else if (flags & BASS_LEVEL_STEREO)
		levels.resize(2);
	else if (flags & BASS_LEVEL_MONO)
		levels.resize(1);

	if (BASS_ChannelGetLevelEx(handle, &levels[0], length, flags))
	{
		sq_newarray(vm, levels.size());

		for (int i = 0; i < levels.size(); ++i)
		{
			sq_pushinteger(vm, i);
			sq_pushinteger(vm, levels[i]);
			sq_set(vm, -3);
		}
	}
	else
		sq_pushnull(vm);

	return 1;
}