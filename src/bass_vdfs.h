#pragma once

#include <bass.h>

void __stdcall BASS_VDF_FileCloseProc(void* user);
QWORD __stdcall BASS_VDF_FileLenProc(void* user);
DWORD __stdcall BASS_VDF_FileReadProc(void* buffer, DWORD length, void* user);
bool __stdcall BASS_VDF_FileSeekProc(QWORD offset, void* user);