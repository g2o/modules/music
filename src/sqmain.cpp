﻿#include <sqapi.h>
#include <bass.h>

#include "sqratGlobalFuncStdcall.h"
#include "sqFunction.h"

#include "bass_error_message.h"

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);
	Sqrat::DefaultVM::Set(vm);

	/* squirreldoc (class)
	*
	* This class represents BASS Sample information.
	*
	* @version	0.2
	* @side		client
	* @name		BassSample
	*
	*/
	/* squirreldoc (property)
	*
	* Represents the used frequency by the sample.
	*
	* @version  0.2
	* @name     freq
	* @return   (int)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents the used volume by the sample.
	*
	* @version  0.2
	* @name     volume
	* @return   (float)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents the used pan balance by the sample.
	*
	* @version  0.2
	* @name     pan
	* @return   (float)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents the used flags by the sample.
	*
	* @version  0.2
	* @name     flags
	* @return   (int)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents the sample length in bytes.
	*
	* @version  0.2
	* @name     length
	* @return   (int)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents the sample maximum simultaneous playbacks for the sample.
	*
	* @version  0.2
	* @name     max
	* @return   (int)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents original resolution bits used by the sample.
	*
	* @version  0.2
	* @name     origres
	* @return   (int)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents the number of sample channels.
	*
	* @version  0.2
	* @name     chans
	* @return   (int)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents the minimum gap in miliseconds between creating channels.
	*
	* @version  0.2
	* @name     mingap
	* @return   (int)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents used 3d mode (`BASS_MODE3D*`) by the sample.
	*
	* @version  0.2
	* @name     mode3d
	* @return   (int)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents sample minimum distance (3d).
	*
	* @version  0.2
	* @name     mindist
	* @return   (float)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents sample maximum distance (3d).
	*
	* @version  0.2
	* @name     maxdist
	* @return   (float)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents sample angle of inside projection cone (3d).
	*
	* @version  0.2
	* @name     iangle
	* @return   (int)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents sample angle of outside projection cone (3d).
	*
	* @version  0.2
	* @name     oangle
	* @return   (int)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents sample delta-volume outside the projection cone (3d).
	*
	* @version  0.2
	* @name     outvol
	* @return   (float)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents sample delta-volume outside the projection cone (3d).
	*
	* @version  0.2
	* @name     outvol
	* @return   (float)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents sample voice allocation/management flags (`BASS_VAM_*`).
	*
	* @version  0.2
	* @name     vam
	* @return   (int)
	*
	*/
	/* squirreldoc (property)
	*
	* Represents sample priority (`0` = lowest, `0xffffffff` = highest).
	*
	* @version  0.2
	* @name     priority
	* @return   (int)
	*
	*/
	Sqrat::Class<BASS_SAMPLE> bass_sample(vm, "BassSample");

	Sqrat::RootTable roottable(vm);
	roottable.Bind("BassSample", bass_sample);

	roottable
		/* squirreldoc (func)
		*
		* This function sets the value of a config option.
		*
		* @version	0.2
		* @category Config
		* @side		client
		* @name		BASS_SetConfig
		* @param	(int) option the option to set the value. For more information see [Config constants](../../../client-constants/config/)
		* @param	(int) value the new option value.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_SetConfig", BASS_SetConfig)
		/* squirreldoc (func)
		*
		* This function gets the value of a config option.
		*
		* @version	0.2
		* @category Config
		* @side		client
		* @name		BASS_GetConfig
		* @param	(int) option the option to set the value. For more information see [Config constants](../../../client-constants/config/)
		* @return	(int) If successful, the value of the requested config option is returned, else `-1` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_GetConfig", BASS_GetConfig)
		/* squirreldoc (func)
		*
		* This function sets the value of a pointer config option.
		*
		* @version	0.2
		* @category Config
		* @side		client
		* @name		BASS_SetConfigPtr
		* @param	(int) option the option to set the value, can be `BASS_CONFIG_NET_AGENT` or `BASS_CONFIG_NET_PROXY`. For more information see [Config constants](../../../client-constants/config/).
		* @param	(string) value the new option value.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_SetConfigPtr", sq_bass_setconfigptr, 3, ".is")
		/* squirreldoc (func)
		*
		* This function gets the value of a pointer config option.
		*
		* @version	0.2
		* @category Config
		* @side		client
		* @name		BASS_GetConfigPtr
		* @param	(int) option the option to set the value, can be `BASS_CONFIG_NET_AGENT` or `BASS_CONFIG_NET_PROXY`. For more information see [Config constants](../../../client-constants/config/).
		* @return	(bool) If successful, the value of the requested config option is returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_GetConfigPtr", sq_bass_getconfigptr, 2, ".i")
		/* squirreldoc (func)
		*
		* This function gets the version of BASS that is loaded.
		*
		* @version	0.2
		* @category Info
		* @side		client
		* @name		BASS_GetVersion
		* @return	(int) The BASS version. For example, `0x02040103` (hex), would be version 2.4.1.3 
		*
		*/
		.Func("BASS_GetVersion", BASS_GetVersion)
		/* squirreldoc (func)
		*
		* This function gets the error code for most recent BASS function call in the current thread.
		*
		* @version	0.2
		* @category Error
		* @side		client
		* @name		BASS_ErrorGetCode
		* @return	(int) The error code. For more information see [Error constants](../../../client-constants/error/).
		*
		*/
		.Func("BASS_ErrorGetCode", BASS_ErrorGetCode)
		/* squirreldoc (func)
		*
		* This function gets the error message for most recent BASS function call in the current thread.
		*
		* @version	0.2
		* @category Error
		* @side		client
		* @name		BASS_ErrorGetMessage
		* @return	(string) The error message.
		*
		*/
		.Func("BASS_ErrorGetMessage", BASS_ErrorGetMessage)
		/* squirreldoc (func)
		*
		* This function gets the information about the output device.
		*
		* @version	0.2
		* @category Info
		* @side		client
		* @name		BASS_GetDeviceInfo
		* @param	(int) device the device to get the information of... `0` = first.
		* @return	({name, driver, flags}) The information about certain device or `null` if device doesn't exists.
		*
		*/
		.SquirrelFunc("BASS_GetDeviceInfo", sq_bass_getdeviceinfo, 2, ".i")
		/* squirreldoc (func)
		*
		* This function initializes an output device.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Init
		* @param	(int) device the device to use... `-1` = default device, `0` = no sound, `1` = first real.
		* @param	(int) freq ouput sample rate.
		* @param	(int) flags a combination of device flags. For more information see [Device constants](../../../client-constants/device/).
		* @return	(bool) If the device was successfully initialized, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_Init", sq_bass_init, 4, ".iii")
		/* squirreldoc (func)
		*
		* This function sets the device to use for subsequent calls in the current thread.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_SetDevice
		* @param	(int) device the device to use... `0` = no sound, `1` = first real output device.
		* @return	(bool) If successful, then `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_SetDevice", BASS_SetDevice)
		/* squirreldoc (func)
		*
		* This function gets the device setting of the current thread.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_GetDevice
		* @return	(bool) If successful, the device number is returned, else `-1` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_GetDevice", BASS_GetDevice)
		/* squirreldoc (func)
		*
		* This function frees all resources used by the output device, including all its samples, streams and MOD musics. Call it only when closing the game.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Free
		* @return	(bool) If successful, then `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Free", BASS_Free)
		/* squirreldoc (func)
		*
		* This function gets a pointer to a DirectSound object interface.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_GetDSoundObject
		* @param	(int) object the interface to retrieve channel/music/stream handle or [BASS_OBJECT_DS](../../../client-constants/object/) or [BASS_OBJECT_DS3DL](../../../client-constants/object/).
		* @return	(userpointer) If successful, then a pointer to the requested object is returned, else `nullptr` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_GetDSoundObject", BASS_GetDSoundObject)
		/* squirreldoc (func)
		*
		* This function gets the information on the device being used.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_GetInfo
		* @return	({flags, hwsize, hwfree, freesam, free3d, minrate, maxrate, eax, minbuf, dsver, latency, initflags, speakers, freq}) If successful, returns a structure containing information about used device, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_GetInfo", sq_bass_getinfo)
		/* squirreldoc (func)
		*
		* This function updates the stream and music channel playback buffers.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Update
		* @param	(int) length the amount of data to render, in miliseconds.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Update", BASS_Update)
		/* squirreldoc (func)
		*
		* This function gets the current CPU usage of BASS.
		*
		* @version	0.2
		* @category Info
		* @side		client
		* @name		BASS_GetCPU
		* @return	(float) the BASS CPU usage as percentage.
		*
		*/
		.Func("BASS_GetCPU", BASS_GetCPU)
		/* squirreldoc (func)
		*
		* This function starts (or resumes) the output.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Start
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Start", BASS_Start)
		/* squirreldoc (func)
		*
		* This function stops the output, stopping all musics/samples/streams on it.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Stop
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Stop", BASS_Stop)
		/* squirreldoc (func)
		*
		* This function stops the output, pausing all musics/samples/streams on it.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_Pause
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Pause", BASS_Pause)
		/* squirreldoc (func)
		*
		* This function sets the output master volume.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_SetVolume
		* @note		This function affects the volume level of all applications using the same output device. If you wish to only affect the level of your application's sounds, the `BASS_ATTRIB_VOL` attribute and/or the `BASS_CONFIG_GVOL_MUSIC` / `BASS_CONFIG_GVOL_SAMPLE` / `BASS_CONFIG_GVOL_STREAM` config options should be used instead. 
		* @param	(int) volume the volume level... `0.0` (silent) to `1.0` (max). 
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_SetVolume", BASS_SetVolume)
		/* squirreldoc (func)
		*
		* This function gets the output master volume.
		*
		* @version	0.2
		* @category Init
		* @side		client
		* @name		BASS_GetVolume
		* @return	(float) If successful, the volume level is returned, else `-1` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_GetVolume", BASS_GetVolume)

		/* squirreldoc (func)
		*
		* This function sets the factors that affect the calculations of 3D sound.
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_Set3DFactors
		* @param	(float) distf the distance factor... `0.0` or less = leave current... examples: `1.0` = use meters, `0.9144` = use yards, `0.3048` = use feet. By default BASS measures distances in meters, you can change this setting if you are using a different unit of measurement.
		* @param	(float) rollf the rolloff factor, how fast the sound quietens with distance... `0.0` (min) - `10.0` (max), less than `0.0` = leave current... examples: `0.0` = no rolloff, `1.0` = real world, `2.0` = 2x real.
		* @param	(float) doppf the doppler factor... `0.0` (min) - `10.0` (max), less than `0.0` = leave current... examples: `0.0` = no doppler, `1.0` = real world, `2.0` = 2x real.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_Set3DFactors", BASS_Set3DFactors)
		/* squirreldoc (func)
		*
		* This function gets the factors that affect the calculations of 3D sound.
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_Get3DFactors
		* @return	({distf, rollf, doppf}) If successful, the factors information is returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_Get3DFactors", sq_bass_get3dfactors, 1, ".")
		/* squirreldoc (func)
		*
		* This fucntion sets the position, velocity, and orientation of the listener (ie. the player).
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_Set3DPosition
		* @param	(Vec3) pos the position of the listener.
		* @param	(Vec3) vel the listener's velocity.
		* @param	(Vec3) front the listener's poinitng direction.
		* @param	(Vec3) top the listener's up direction.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_Set3DPosition", sq_bass_set3dposition, 5, ".")
		/* squirreldoc (func)
		*
		* This function gets the position, velocity, and orientation of the listener (ie. the player).
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_Get3DPosition
		* @return	({pos, vel, front, top}) If successful, the 3d position vectors are returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_Get3DPosition", sq_bass_get3dposition, 1, ".")
		/* squirreldoc (func)
		*
		* This function applies changes made to the 3D system.
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_Apply3D
		*
		*/
		.Func("BASS_Apply3D", BASS_Apply3D)
		/* squirreldoc (func)
		*
		* This function sets the type of EAX environment and its parameters. 
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_SetEAXParameters
		* @param	(int) env the eax enviorment... `-1` = leave current. For more information see [EAX constants](../../../client-constants/eax/)
		* @param	(float) vol the volume of the reverb... `0.0` (off) to `1.0` (max), less than `0.0` = leave current.
		* @param	(float) decay the time in seconds it takes the reverb to diminish by 60 dB... `0.1` (min) to `20.0` (max), less than `0.0` = leave current.
		* @param	(float) damp the time in seconds it takes the reverb to diminish by 60 dB... `0.1` (min) to 20 (max), less than `0.0` = leave current. 
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_SetEAXParameters", BASS_SetEAXParameters)
		/* squirreldoc (func)
		*
		* This function gets the current type of EAX environment and its parameters.
		*
		* @version	0.2
		* @category Surrounding
		* @side		client
		* @name		BASS_GetEAXParameters
		* @return	({env, vol, decay, damp}) If successful, the eax informations are returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_GetEAXParameters", sq_bass_geteaxparameters, 1, ".")

		/* squirreldoc (func)
		*
		* This function loads a MOD music file. 
		*
		* @version	0.2
		* @category Music
		* @side		client
		* @name		BASS_MusicLoad
		* @param	(string) file the file path to a MOD music file.
		* @param	(int) offset the file offset to load the MOD music from.
		* @param	(int) length the data length... `0` = use all data up to the end of file.
		* @param	(int) flags the combination of [Sample Flags](../../../client-constants/sample/) and [Music Flags](../../../client-constants/music/).
		* @param	(int) freq sample rate to render/play the MOD music at... `0` = the rate specified in the [BASS_Init](../../../client-functions/init/BASS_Init) call, `1` = the device's current output rate (or the [BASS_Init](../../../client-functions/init/BASS_Init) rate if that is not available).
		* @return	(int) If successful, the loaded MOD music's handle is returned, else `0` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_MusicLoad", sq_bass_musicload, 6, ".siiii")
		/* squirreldoc (func)
		*
		* This function frees a MOD music's resources, including any sync/DSP/FX it has. 
		*
		* @version	0.2
		* @category Music
		* @side		client
		* @name		BASS_MusicFree
		* @param	(int) handle the MOD music handle.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_MusicFree", BASS_MusicFree)

		/* squirreldoc (func)
		*
		* This function loads a WAV, AIFF, MP3, MP2, MP1, OGG or plugin supported sample. 
		*
		* @version	0.2
		* @category Sample
		* @side		client
		* @name		BASS_MusicLoad
		* @param	(string) file the file path.
		* @param	(int) offset the file offset to load the sample from.
		* @param	(int) length the data length... `0` = use all data up to the end of file.
		* @param	(int) max the maximum number of simultaneous playbacks... `1` (min) - `65535` (max). 
		* @param	(int) flags For more information see [Sample Flags](../../../client-constants/sample/).
		* @return	(int) If successful, the loaded sample handle is returned, else `0` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_SampleLoad", sq_bass_sampleload, 6, ".siiii")
		/* squirreldoc (func)
		*
		* This function frees sample's resources. 
		*
		* @version	0.2
		* @category Sample
		* @side		client
		* @name		BASS_SampleFree
		* @param	(int) handle the sample handle.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_SampleFree", BASS_SampleFree)
		/* squirreldoc (func)
		*
		* This function gets sample's default attributes and other information. 
		*
		* @version	0.2
		* @category Sample
		* @side		client
		* @name		BASS_SampleGetInfo
		* @param	(int) handle the sample handle.
		* @return	(BassSample) If successful, BassSample object is returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_SampleGetInfo", sq_bass_samplegetinfo, 2, ".i")
		/* squirreldoc (func)
		*
		* This function sets sample's default attributes. 
		*
		* @version	0.2
		* @category Sample
		* @side		client
		* @name		BASS_SampleSetInfo
		* @param	(int) handle the sample handle.
		* @param	(BassSample) info the sample info.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_SampleSetInfo", sq_bass_samplesetinfo, 3, ".ix")
		/* squirreldoc (func)
		*
		* This function creates/initializes a playback channel for a sample. 
		*
		* @version	0.2
		* @category Sample
		* @side		client
		* @name		BASS_SampleGetChannel
		* @param	(int) handle the sample handle.
		* @param	(bool) onlynew do not recycle/override one of the sample's existing channels.
		* @return	(bool) If successful, the handle of the new channel is returned is returned, else `0` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_SampleGetChannel", BASS_SampleGetChannel)
		/* squirreldoc (func)
		*
		* This function gets all of the existing sample's channels. 
		*
		* @version	0.2
		* @category Sample
		* @side		client
		* @name		BASS_SampleGetChannels
		* @param	(int) handle the sample handle.
		* @return	([channel...]) If successful, the the array containing channel handles is returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_SampleGetChannels", sq_bass_samplegetchannels, 2, ".i")
		/* squirreldoc (func)
		*
		* This function stops all instances of a sample. 
		*
		* @version	0.2
		* @category Sample
		* @side		client
		* @name		BASS_SampleStop
		* @param	(int) handle the sample handle.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_SampleStop", BASS_SampleStop)

		/* squirreldoc (func)
		*
		* This function creates a sample stream from an MP3, MP2, MP1, OGG, WAV, AIFF or plugin supported file. 
		*
		* @version	0.2
		* @category Sample
		* @side		client
		* @name		BASS_StreamCreateFile
		* @param	(string) filename the file path to music file from VDF archive (`_Work/Data/Sound` or `_Work/Data/Music`).
		* @param	(int) flags For more information see [Sample flags](../../../client-constants/sample/).
		* @return	(int) If successful, stream handle is returned, else `0` is returend. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_StreamCreateFile", sq_bass_streamcreatefile, 3, ".si")
		/* squirreldoc (func)
		*
		* This function creates a sample stream from an MP3, MP2, MP1, OGG, WAV, AIFF or plugin supported file on the internet. 
		*
		* @version	0.2
		* @category Sample
		* @side		client
		* @name		BASS_StreamCreateURL
		* @param	(string) url url of the file to stream. Should begin with `http://` or `https://` or `ftp://`, or another add-on supported protocol. The URL can be followed by custom HTTP request headers to be sent to the server; the URL and each header should be terminated with a carriage return and line feed `\r\n`.
		* @param	(int) flags a combination of [Sample flags](../../../client-constants/sample/) and [Stream flags](../../../client-constants/stream/).
		* @return	(int) If successful, stream handle is returned, else `0` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_StreamCreateURL", sq_bass_streamcreateurl, 4, ".sii")
		/* squirreldoc (func)
		*
		* This function frees a sample stream's resources, including any sync/DSP/FX it has. 
		*
		* @version	0.2
		* @category Sample
		* @side		client
		* @name		BASS_StreamFree
		* @param	(int) handle the stream handle.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_StreamFree", BASS_StreamFree)
		/* squirreldoc (func)
		*
		* This function gets the file position/status of a stream. 
		*
		* @version	0.2
		* @category Sample
		* @side		client
		* @name		BASS_StreamGetFilePosition
		* @param	(int) handle the stream handle.
		* @param	(int) mode For more information see [File position flags](../../../client-constants/file-position/).
		* @return	(bool) If successful, then the requested file position/status is returned, else `-1` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_StreamGetFilePosition", BASS_StreamGetFilePosition)

		/* squirreldoc (func)
		*
		* This function translates a byte position into time (seconds), based on a channel's format.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelBytes2Seconds
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) pos The position to translate.
		* @return	(int) If successful, then the translated length is returned, else a negative value is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelBytes2Seconds", BASS_ChannelBytes2Seconds)
		/* squirreldoc (func)
		*
		* This function translates a time (seconds) position into bytes, based on a channel's format.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelSeconds2Bytes
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) pos The position to translate.
		* @return	(int) If successful, then the translated length is returned, else -1 is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelSeconds2Bytes", BASS_ChannelSeconds2Bytes)
		/* squirreldoc (func)
		*
		* This function retrieves the device that a channel is using.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelGetDevice
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @return	(int) If successful, the device number is returned, else -1 is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelGetDevice", BASS_ChannelGetDevice)
		/* squirreldoc (func)
		*
		* This function changes the device that a stream, MOD music or sample is using.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelSetDevice
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) device The device to use... 0 = no sound, 1 = first real output device, `BASS_NODEVICE` = no device.
		* @return	(int) If successful, the device number is returned, else -1 is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelSetDevice", BASS_ChannelSetDevice)
		/* squirreldoc (func)
		*
		* This function checks if a sample, stream, or MOD music is active (playing) or stalled.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelIsActive
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @return	(int) the state of channel. For more information see [channel states](../../../client-constants/Active/).
		*
		*/
		.Func("BASS_ChannelIsActive", BASS_ChannelIsActive)
		/* squirreldoc (func)
		*
		* This function retrieves information on a channel.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelGetInfo
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @return	({freq, chans, flags, ctype, plugin, origres, sample, filename}) If successful, channel info is returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_ChannelGetInfo", sq_bass_channelgetinfo, 2, ".i")
		/* squirreldoc (func)
		*
		* This function retrieves tags/headers from a channel.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelGetTags
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) tags The tags/headers wanted. For more information see [channel tags](../../../client-constants/Tag/).
		* @return	(array) If successful, the requested tags are returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelGetTags", BASS_ChannelGetTags)
		/* squirreldoc (func)
		*
		* This function modifies and retrieves a channel's flags.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelFlags
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) flags A combination of these flags.
		* @param	(int) mask The flags (as above) to modify. Flags that are not included in this are left as they are, so it can be set to 0 in order to just retrieve the current flags. To modify the speaker flags, any of the BASS_SPEAKER_xxx flags can be used in the mask (no need to include all of them).
		* @return	(int) If successful, the channel's updated flags are returned, else -1 is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelFlags", BASS_ChannelFlags)
		/* squirreldoc (func)
		*
		* This function updates the playback buffer of a stream or MOD music.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelUpdate
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) length The amount of data to render, in milliseconds... 0 = default (2 x update period). This is capped at the space available in the buffer.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelUpdate", BASS_ChannelUpdate)
		/* squirreldoc (func)
		*
		* This function locks a stream, MOD music or recording channel to the current thread.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelLock
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(bool) lock If `true`, unlock the channel, else lock it.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelLock", BASS_ChannelLock)
		/* squirreldoc (func)
		*
		* This function starts/resumes playback of a sample, stream, MOD music, or a recording.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelPlay
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(bool) restart Restart playback from the beginning? This has no effect on streams with the `BASS_STREAM_BLOCK` flag set or on recordings.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelPlay", BASS_ChannelPlay)
		/* squirreldoc (func)
		*
		* This function stops a sample, stream, MOD music, or recording.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelStop
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelStop", BASS_ChannelStop)
		/* squirreldoc (func)
		*
		* This function pauses a sample, stream, MOD music, or recording.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelPause
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelPause", BASS_ChannelPause)
		/* squirreldoc (func)
		*
		* This function sets the value of a channel's attribute.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelSetAttribute
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) attrib The attribute to set. For more information see [channel attributes](../../../client-constants/Attribute/).
		* @param	(float) value The new attribute value. See the attribute's documentation for details on the possible values.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelSetAttribute", BASS_ChannelSetAttribute)
		/* squirreldoc (func)
		*
		* This function retrieves the value of a channel's attribute.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelGetAttribute
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) attrib The attribute to set. For more information see [channel attributes](../../../client-constants/Attribute/).
		* @return	(bool) If successful, attibute value is returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_ChannelGetAttribute", sq_bass_channelgetattribute, 3, ".ii")
		/* squirreldoc (func)
		*
		* This function slides a channel's attribute from its current value to a new value.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelSlideAttribute
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) attrib The attribute to set. For more information see [channel attributes](../../../client-constants/Attribute/).
		* @param	(float) value The new attribute value. See the attribute's documentation for details on the possible values.
		* @param	(int) time The length of time (in milliseconds) that it should take for the attribute to reach the value.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelSlideAttribute", BASS_ChannelSlideAttribute)
		/* squirreldoc (func)
		*
		* This function checks if an attribute (or any attribute) of a sample, stream, or MOD music is sliding.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelIsSliding
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) attrib The attribute to set. For more information see [channel attributes](../../../client-constants/Attribute/).
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelIsSliding", BASS_ChannelIsSliding)
		/* squirreldoc (func)
		*
		* This function sets the 3D attributes of a sample, stream, or MOD music channel with 3D functionality.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelSet3DAttributes
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) mode The 3D processing mode flags, -1 = leave current. For more information see ..........................
		* @param	(float) min The minimum distance. The channel's volume is at maximum when the listener is within this distance... 0 or less = leave current.
		* @param	(float) max The maximum distance. The channel's volume stops decreasing when the listener is beyond this distance... 0 or less = leave current.
		* @param	(int) iangle The angle of the inside projection cone in degrees... 0 (no cone) to 360 (sphere), -1 = leave current.
		* @param	(int) oangle The angle of the outside projection cone in degrees... 0 (no cone) to 360 (sphere), -1 = leave current.
		* @param	(float) outvol The delta-volume outside the outer projection cone... 0 (silent) to 1 (same as inside the cone), less than 0 = leave current.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelSet3DAttributes", BASS_ChannelSet3DAttributes)
		/* squirreldoc (func)
		*
		* This function retrieves the 3D attributes of a sample, stream, or MOD music channel with 3D functionality.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelGet3DAttributes
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @return	(({mode, min, max, iangle, oangle, outvol})) If successful, 3d attribute data is returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_ChannelGet3DAttributes", sq_bass_channelget3dattributes, 2, ".i")
		/* squirreldoc (func)
		*
		* This function sets the 3D position of a sample, stream, or MOD music channel with 3D functionality.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelSet3DPosition
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(Vec3) pos Position of the sound... NULL = leave current.
		* @param	(Vec3) orient Orientation of the sound... NULL = leave current. This is automatically normalized.
		* @param	(Vec3) vel Velocity of the sound... NULL = leave current. This is only used to calculate the doppler effect, and has no effect on the sound's position.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_ChannelSet3DPosition", sq_bass_channelset3dposition, 5, ".i")
		/* squirreldoc (func)
		*
		* This function retrieves the 3D position of a sample, stream, or MOD music channel with 3D functionality.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelGet3DPosition
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @return	(({pos, orient, vel})) If successful, 3d position data is returned, else `null` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.SquirrelFunc("BASS_ChannelGet3DPosition", sq_bass_channelget3dposition, 2, ".i")
		/* squirreldoc (func)
		*
		* This function retrieves the length of a channel.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelGetLength
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) mode How to retrieve the length.
		* @return	(int) If successful, then the channel's length is returned, else -1 is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelGetLength", BASS_ChannelGetLength)
		/* squirreldoc (func)
		*
		* This function sets the current position of a channel.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelSetPosition
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) pos The position, in units determined by the mode.
		* @param	(int) mode How to set the position.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelSetPosition", BASS_ChannelSetPosition)
		/* squirreldoc (func)
		*
		* This function retrieves the current position of a channel.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelGetPosition
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) mode How to retrieve the position.
		* @return	(int) If successful, then the channel's position is returned, else -1 is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelGetPosition", BASS_ChannelGetPosition)
		/* squirreldoc (func)
		*
		* This function retrieves the level (peak amplitude) of a sample, stream, MOD music, or recording channel.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelGetLevel
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @return	(int) If successful, the level of the left channel is returned in the low word (low 16 bits), and the level of the right channel is returned in the high word (high 16 bits). If the channel is mono, then the low word is duplicated in the high word. The level ranges linearly from 0 (silent) to 32768 (max). 0 will be returned when a channel is stalled. If an error occurs, -1 is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelGetLevel", BASS_ChannelGetLevel)
		/* squirreldoc (func)
		*
		* This function retrieves the level of a sample, stream, MOD music, or recording channel.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelGetLevelEx
		*
		*/
		.SquirrelFunc("BASS_ChannelGetLevelEx", sq_bass_channelgetlevelex, 4, ".ifi")
		/* squirreldoc (func)
		*
		* This function links two MOD music or stream channels together.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelSetLink
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) chan The handle of the channel to have linked with it.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelSetLink", BASS_ChannelSetLink)
		/* squirreldoc (func)
		*
		* This function removes a links between two MOD music or stream channels.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelRemoveLink
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) chan The handle of the channel to have unlinked with it.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelRemoveLink", BASS_ChannelRemoveLink)
		/* squirreldoc (func)
		*
		* This function sets an effect on a stream, MOD music, or recording channel.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelSetFX
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) type Effect type
		* @param	(int) priority The priority of the new FX, which determines its position in the DSP chain. DSP/FX with higher priority are applied before those with lower.
		* @return	(bool) If successful, then the new effect's handle is returned, else 0 is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelSetFX", BASS_ChannelSetFX)
		/* squirreldoc (func)
		*
		* This function removes an effect on a stream, MOD music, or recording channel.
		*
		* @version	0.2
		* @category Channel
		* @side		client
		* @name		BASS_ChannelRemoveFX
		* @param	(int) handle The channel handle (channel, music, stream or record)
		* @param	(int) fx Handle of the effect to remove from the channel.
		* @return	(bool) If successful, `true` is returned, else `false` is returned. Use [BASS_ErrorGetCode](../../../client-functions/error/BASS_ErrorGetCode) or [BASS_ErrorGetMessage](../../../client-functions/error/BASS_ErrorGetMessage) to get more info.
		*
		*/
		.Func("BASS_ChannelRemoveFX", BASS_ChannelRemoveFX)
	;

	Sqrat::ConstTable(vm)
		/* squirreldoc (const)
		*
		* Represents current BASS Version as integer.
		*
		* @category Info
		* @side		client
		* @name		BASSVERSION
		*
		*/
		.Const("BASSVERSION", BASSVERSION)

		/* squirreldoc (const)
		*
		* Represents current BASS Version as text.
		*
		* @category Info
		* @side		client
		* @name		BASSVERSIONTEXT
		*
		*/
		.Const("BASSVERSIONTEXT", BASSVERSIONTEXT)

		/* squirreldoc (const)
		*
		* Represents ok status (no error).
		*
		* @category Error
		* @side		client
		* @name		BASS_OK
		*
		*/
		.Const("BASS_OK", BASS_OK)
		/* squirreldoc (const)
		*
		* Represents insufficient memory error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_MEM
		*
		*/
		.Const("BASS_ERROR_MEM", BASS_ERROR_MEM)
		/* squirreldoc (const)
		*
		* Represents unable to open file error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_FILEOPEN
		*
		*/
		.Const("BASS_ERROR_FILEOPEN", BASS_ERROR_FILEOPEN)
		/* squirreldoc (const)
		*
		* Represents no device driver or device already used error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_DRIVER
		*
		*/
		.Const("BASS_ERROR_DRIVER", BASS_ERROR_DRIVER)
		/* squirreldoc (const)
		*
		* Represents sample buffer lost error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_BUFLOST
		*
		*/
		.Const("BASS_ERROR_BUFLOST", BASS_ERROR_BUFLOST)
		/* squirreldoc (const)
		*
		* Represents invalid handle passed error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_HANDLE
		*
		*/
		.Const("BASS_ERROR_HANDLE", BASS_ERROR_HANDLE)
		/* squirreldoc (const)
		*
		* Represents sample format not being supported by the device/drivers error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_FORMAT
		*
		*/
		.Const("BASS_ERROR_FORMAT", BASS_ERROR_FORMAT)
		/* squirreldoc (const)
		*
		* Represents invalid position passed error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_POSITION
		*
		*/
		.Const("BASS_ERROR_POSITION", BASS_ERROR_POSITION)
		/* squirreldoc (const)
		*
		* Represents device not initialized error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_INIT
		*
		*/
		.Const("BASS_ERROR_INIT", BASS_ERROR_INIT)
		/* squirreldoc (const)
		*
		* Represents output is paused/stopped error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_START
		*
		*/
		.Const("BASS_ERROR_START", BASS_ERROR_START)
		/* squirreldoc (const)
		*
		* Represents SSL/HTTPS support is not available error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_SSL
		*
		*/
		.Const("BASS_ERROR_SSL", BASS_ERROR_SSL)
		/* squirreldoc (const)
		*
		* Represents certain action was done already error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_ALREADY
		*
		*/
		.Const("BASS_ERROR_ALREADY", BASS_ERROR_ALREADY)
		/* squirreldoc (const)
		*
		* Represents sample has no free channels error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOCHAN
		*
		*/
		.Const("BASS_ERROR_NOCHAN", BASS_ERROR_NOCHAN)
		/* squirreldoc (const)
		*
		* Represents illegal type being passed error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_ILLTYPE
		*
		*/
		.Const("BASS_ERROR_ILLTYPE", BASS_ERROR_ILLTYPE)
		/* squirreldoc (const)
		*
		* Represents illegal parameter being passed error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_ILLPARAM
		*
		*/
		.Const("BASS_ERROR_ILLPARAM", BASS_ERROR_ILLPARAM)
		/* squirreldoc (const)
		*
		* Represents 3d functionallity not initialized/supported error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NO3D
		*
		*/
		.Const("BASS_ERROR_NO3D", BASS_ERROR_NO3D)
		/* squirreldoc (const)
		*
		* Represents EAX functionallity not supported error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOEAX
		*
		*/
		.Const("BASS_ERROR_NOEAX", BASS_ERROR_NOEAX)
		/* squirreldoc (const)
		*
		* Represents invalid device error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_DEVICE
		*
		*/
		.Const("BASS_ERROR_DEVICE", BASS_ERROR_DEVICE)
		/* squirreldoc (const)
		*
		* Represents channel not playing error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOPLAY
		*
		*/
		.Const("BASS_ERROR_NOPLAY", BASS_ERROR_NOPLAY)
		/* squirreldoc (const)
		*
		* Represents illegal sample rate error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_FREQ
		*
		*/
		.Const("BASS_ERROR_FREQ", BASS_ERROR_FREQ)
		/* squirreldoc (const)
		*
		* Represents stream is not a file stream error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOTFILE
		*
		*/
		.Const("BASS_ERROR_NOTFILE", BASS_ERROR_NOTFILE)
		/* squirreldoc (const)
		*
		* Represents no hardware voices available error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOHW
		*
		*/
		.Const("BASS_ERROR_NOHW", BASS_ERROR_NOHW)
		/* squirreldoc (const)
		*
		* Represents MOD music has no sequence data error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_EMPTY
		*
		*/
		.Const("BASS_ERROR_EMPTY", BASS_ERROR_EMPTY)
		/* squirreldoc (const)
		*
		* Represents no internet connection could be opened error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NONET
		*
		*/
		.Const("BASS_ERROR_NONET", BASS_ERROR_NONET)
		/* squirreldoc (const)
		*
		* Represents couldn't create file error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_CREATE
		*
		*/
		.Const("BASS_ERROR_CREATE", BASS_ERROR_CREATE)
		/* squirreldoc (const)
		*
		* Represents effects are not available error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOFX
		*
		*/
		.Const("BASS_ERROR_NOFX", BASS_ERROR_NOFX)
		/* squirreldoc (const)
		*
		* Represents requested data is not available error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_NOTAVAIL
		*
		*/
		.Const("BASS_ERROR_NOTAVAIL", BASS_ERROR_NOTAVAIL)
		/* squirreldoc (const)
		*
		* Represents channel is/isn't a "decoding channel" error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_DECODE
		*
		*/
		.Const("BASS_ERROR_DECODE", BASS_ERROR_DECODE)
		/* squirreldoc (const)
		*
		* Represents sufficient version of DirectX is not installed error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_DX
		*
		*/
		.Const("BASS_ERROR_DX", BASS_ERROR_DX)
		/* squirreldoc (const)
		*
		* Represents connection/sample timedout error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_TIMEOUT
		*
		*/
		.Const("BASS_ERROR_TIMEOUT", BASS_ERROR_TIMEOUT)
		/* squirreldoc (const)
		*
		* Represents file format not recognised/supported error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_FILEFORM
		*
		*/
		.Const("BASS_ERROR_FILEFORM", BASS_ERROR_FILEFORM)
		/* squirreldoc (const)
		*
		* Represents speaker unavailable error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_SPEAKER
		*
		*/
		.Const("BASS_ERROR_SPEAKER", BASS_ERROR_SPEAKER)
		/* squirreldoc (const)
		*
		* Represents invalid BASS version (used by add-ons) error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_VERSION
		*
		*/
		.Const("BASS_ERROR_VERSION", BASS_ERROR_VERSION)
		/* squirreldoc (const)
		*
		* Represents codec is not available/supported error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_CODEC
		*
		*/
		.Const("BASS_ERROR_CODEC", BASS_ERROR_CODEC)
		/* squirreldoc (const)
		*
		* Represents channel/file has ended error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_ENDED
		*
		*/
		.Const("BASS_ERROR_ENDED", BASS_ERROR_ENDED)
		/* squirreldoc (const)
		*
		* Represents device is busy error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_BUSY
		*
		*/
		.Const("BASS_ERROR_BUSY", BASS_ERROR_BUSY)
		/* squirreldoc (const)
		*
		* Represents unknown error.
		*
		* @category Error
		* @side		client
		* @name		BASS_ERROR_BUSY
		*
		*/
		.Const("BASS_ERROR_UNKNOWN", BASS_ERROR_UNKNOWN)

		/* squirreldoc (const)
		*
		* Represents playback buffer length.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_BUFFER
		*
		*/
		.Const("BASS_CONFIG_BUFFER", BASS_CONFIG_BUFFER)
		/* squirreldoc (const)
		*
		* Represents update period of playback buffers.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_UPDATEPERIOD
		*
		*/
		.Const("BASS_CONFIG_UPDATEPERIOD", BASS_CONFIG_UPDATEPERIOD)
		/* squirreldoc (const)
		*
		* Represents global sample volume.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_GVOL_SAMPLE
		*
		*/
		.Const("BASS_CONFIG_GVOL_SAMPLE", BASS_CONFIG_GVOL_SAMPLE)
		/* squirreldoc (const)
		*
		* Represents global stream volume.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_GVOL_STREAM
		*
		*/
		.Const("BASS_CONFIG_GVOL_STREAM", BASS_CONFIG_GVOL_STREAM)
		/* squirreldoc (const)
		*
		* Represents global MOD music volume.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_GVOL_MUSIC
		*
		*/
		.Const("BASS_CONFIG_GVOL_MUSIC", BASS_CONFIG_GVOL_MUSIC)
		/* squirreldoc (const)
		*
		* Represents volume translation curve.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_CURVE_VOL
		*
		*/
		.Const("BASS_CONFIG_CURVE_VOL", BASS_CONFIG_CURVE_VOL)
		/* squirreldoc (const)
		*
		* Represents panning translation curve.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_CURVE_PAN
		*
		*/
		.Const("BASS_CONFIG_CURVE_PAN", BASS_CONFIG_CURVE_PAN)
		/* squirreldoc (const)
		*
		* Represents passing 32-bit floating-point sample data to all DSP functions.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_FLOATDSP
		*
		*/
		.Const("BASS_CONFIG_FLOATDSP", BASS_CONFIG_FLOATDSP)
		/* squirreldoc (const)
		*
		* Represents the 3D algorithm for software mixed 3D channels.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_3DALGORITHM
		*
		*/
		.Const("BASS_CONFIG_3DALGORITHM", BASS_CONFIG_3DALGORITHM)
		/* squirreldoc (const)
		*
		* Represents time to wait for a server to respond to a connection request.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_TIMEOUT
		*
		*/
		.Const("BASS_CONFIG_NET_TIMEOUT", BASS_CONFIG_NET_TIMEOUT)
		/* squirreldoc (const)
		*
		* Represents internet download buffer length.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_BUFFER
		*
		*/
		.Const("BASS_CONFIG_NET_BUFFER", BASS_CONFIG_NET_BUFFER)
		/* squirreldoc (const)
		*
		* Represents preventing channels being played when the output is paused.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_PAUSE_NOPLAY
		*
		*/
		.Const("BASS_CONFIG_PAUSE_NOPLAY", BASS_CONFIG_PAUSE_NOPLAY)
		/* squirreldoc (const)
		*
		* Represents amount to pre-buffer when opening internet streams.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_PREBUF
		*
		*/
		.Const("BASS_CONFIG_NET_PREBUF", BASS_CONFIG_NET_PREBUF)
		/* squirreldoc (const)
		*
		* Represents using passive mode in FTP connections.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_PASSIVE
		*
		*/
		.Const("BASS_CONFIG_NET_PASSIVE", BASS_CONFIG_NET_PASSIVE)
		/* squirreldoc (const)
		*
		* Represents processing URLs in playlists.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_PLAYLIST
		*
		*/
		.Const("BASS_CONFIG_NET_PLAYLIST", BASS_CONFIG_NET_PLAYLIST)
		/* squirreldoc (const)
		*
		* Represents maximum number of virtual channels to use in the rendering of IT files.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_MUSIC_VIRTUAL
		*
		*/
		.Const("BASS_CONFIG_MUSIC_VIRTUAL", BASS_CONFIG_MUSIC_VIRTUAL)
		/* squirreldoc (const)
		*
		* Represents the amount of data to check in order to verify/detect the file format.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_VERIFY
		*
		*/
		.Const("BASS_CONFIG_VERIFY", BASS_CONFIG_VERIFY)
		/* squirreldoc (const)
		*
		* Represents the number of threads to use for updating playback buffers.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_UPDATETHREADS
		*
		*/
		.Const("BASS_CONFIG_UPDATETHREADS", BASS_CONFIG_UPDATETHREADS)
		/* squirreldoc (const)
		*
		* Represents the output device buffer length.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_DEV_BUFFER
		*
		*/
		.Const("BASS_CONFIG_DEV_BUFFER", BASS_CONFIG_DEV_BUFFER)
		/* squirreldoc (const)
		*
		* Represents enabling true play position mode on Windows Vista and newer.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_VISTA_TRUEPOS
		*
		*/
		.Const("BASS_CONFIG_VISTA_TRUEPOS", BASS_CONFIG_VISTA_TRUEPOS)
		/* squirreldoc (const)
		*
		* Represents inclusion of a `Default` entry in the output device list.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_DEV_DEFAULT
		*
		*/
		.Const("BASS_CONFIG_DEV_DEFAULT", BASS_CONFIG_DEV_DEFAULT)
		/* squirreldoc (const)
		*
		* Represents the time to wait for a server to deliver more data for an internet stream.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_READTIMEOUT
		*
		*/
		.Const("BASS_CONFIG_NET_READTIMEOUT", BASS_CONFIG_NET_READTIMEOUT)
		/* squirreldoc (const)
		*
		* Represents enabling speaker assignment with panning/balance control on Windows Vista and newer.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_VISTA_SPEAKERS
		*
		*/
		.Const("BASS_CONFIG_VISTA_SPEAKERS", BASS_CONFIG_VISTA_SPEAKERS)
		/* squirreldoc (const)
		*
		* Represents disabling the use of Media Foundation.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_MF_DISABLE
		*
		*/
		.Const("BASS_CONFIG_MF_DISABLE", BASS_CONFIG_MF_DISABLE)
		/* squirreldoc (const)
		*
		* Represents the number of existing HMUSIC / HRECORD / HSAMPLE / HSTREAM handle.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_HANDLES
		*
		*/
		.Const("BASS_CONFIG_HANDLES", BASS_CONFIG_HANDLES)
		/* squirreldoc (const)
		*
		* Represents the usage of Unicode character set in device information.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_UNICODE
		*
		*/
		.Const("BASS_CONFIG_UNICODE", BASS_CONFIG_UNICODE)
		/* squirreldoc (const)
		*
		* Represents the default sample rate conversion quality.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_SRC
		*
		*/
		.Const("BASS_CONFIG_SRC", BASS_CONFIG_SRC)
		/* squirreldoc (const)
		*
		* Represents the default sample rate conversion quality for samples.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_SRC_SAMPLE
		*
		*/
		.Const("BASS_CONFIG_SRC_SAMPLE", BASS_CONFIG_SRC_SAMPLE)
		/* squirreldoc (const)
		*
		* Represents the buffer length for asynchronous file reading.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_ASYNCFILE_BUFFER
		*
		*/
		.Const("BASS_CONFIG_ASYNCFILE_BUFFER", BASS_CONFIG_ASYNCFILE_BUFFER)
		/* squirreldoc (const)
		*
		* Represents Pre-scanning chained OGG files.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_OGG_PRESCAN
		*
		*/
		.Const("BASS_CONFIG_OGG_PRESCAN", BASS_CONFIG_OGG_PRESCAN)
		/* squirreldoc (const)
		*
		* Represents playing the audio from video files using Media Foundation.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_MF_VIDEO
		*
		*/
		.Const("BASS_CONFIG_MF_VIDEO", BASS_CONFIG_MF_VIDEO)
		/* squirreldoc (const)
		*
		* Represents not stopping the output device when nothing is playing on it.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_DEV_NONSTOP
		*
		*/
		.Const("BASS_CONFIG_DEV_NONSTOP", BASS_CONFIG_DEV_NONSTOP)
		/* squirreldoc (const)
		*
		* Represents the amount of data to check in order to verify/detect the file format of internet streams.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_VERIFY_NET
		*
		*/
		.Const("BASS_CONFIG_VERIFY_NET", BASS_CONFIG_VERIFY_NET)

		/* squirreldoc (const)
		*
		* Represents the "User-Agent" request header sent to servers.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_AGENT
		*
		*/
		.Const("BASS_CONFIG_NET_AGENT", BASS_CONFIG_NET_AGENT)
		/* squirreldoc (const)
		*
		* Represents the proxy server settings.
		*
		* @category Config
		* @side		client
		* @name		BASS_CONFIG_NET_PROXY
		*
		*/
		.Const("BASS_CONFIG_NET_PROXY", BASS_CONFIG_NET_PROXY)

		/* squirreldoc (const)
		*
		* Represents using 8-bit resolution.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_8BITS
		*
		*/
		.Const("BASS_DEVICE_8BITS", BASS_DEVICE_8BITS)
		/* squirreldoc (const)
		*
		* Represents using mono.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_MONO
		*
		*/
		.Const("BASS_DEVICE_MONO", BASS_DEVICE_MONO)
		/* squirreldoc (const)
		*
		* Represents enabling 3d functionallity.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_3D
		*
		*/
		.Const("BASS_DEVICE_3D", BASS_DEVICE_3D)
		/* squirreldoc (const)
		*
		* Represents calculating device latency (BASS_GetInfo).
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_LATENCY
		*
		*/
		.Const("BASS_DEVICE_LATENCY", BASS_DEVICE_LATENCY)
		/* squirreldoc (const)
		*
		* Represents detecting speakers via Windows control panel.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_CPSPEAKERS
		*
		*/
		.Const("BASS_DEVICE_CPSPEAKERS", BASS_DEVICE_CPSPEAKERS)
		/* squirreldoc (const)
		*
		* Represents force enabling of speaker assignment.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_SPEAKERS
		*
		*/
		.Const("BASS_DEVICE_SPEAKERS", BASS_DEVICE_SPEAKERS)
		/* squirreldoc (const)
		*
		* Represents ignoring speaker arrangement.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_SPEAKERS
		*
		*/
		.Const("BASS_DEVICE_NOSPEAKER", BASS_DEVICE_NOSPEAKER)
		/* squirreldoc (const)
		*
		* Represents using ALSA "dmix" plugin.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_SPEAKERS
		*
		*/
		.Const("BASS_DEVICE_DMIX", BASS_DEVICE_DMIX)
		/* squirreldoc (const)
		*
		* Represents setting device's output rate to freq.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_FREQ
		*
		*/
		.Const("BASS_DEVICE_FREQ", BASS_DEVICE_FREQ)

		/* squirreldoc (const)
		*
		* Represents IDirectSound interface id.
		*
		* @category Object
		* @side		client
		* @name		BASS_OBJECT_DS
		*
		*/
		.Const("BASS_OBJECT_DS", BASS_OBJECT_DS)
		/* squirreldoc (const)
		*
		* Represents IDirectSound3DListener interface id.
		*
		* @category Object
		* @side		client
		* @name		BASS_OBJECT_DS3DL
		*
		*/
		.Const("BASS_OBJECT_DS3DL", BASS_OBJECT_DS3DL)

		/* squirreldoc (const)
		*
		* Represents that device is already enabled.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_ENABLED
		*
		*/
		.Const("BASS_DEVICE_ENABLED", BASS_DEVICE_ENABLED)
		/* squirreldoc (const)
		*
		* Represents using default system device.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_DEFAULT
		*
		*/
		.Const("BASS_DEVICE_DEFAULT", BASS_DEVICE_DEFAULT)
		/* squirreldoc (const)
		*
		* Represents that device is already initialized.
		*
		* @category Device
		* @side		client
		* @name		BASS_DEVICE_INIT
		*
		*/
		.Const("BASS_DEVICE_INIT", BASS_DEVICE_INIT)

		/* squirreldoc (const)
		*
		* Represents bitmask to identify the device type.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_MASK
		*
		*/
		.Const("BASS_DEVICE_TYPE_MASK", static_cast<SQInteger>(BASS_DEVICE_TYPE_MASK))
		/* squirreldoc (const)
		*
		* Represents an audio endpoint device that the user accesses remotely through a network.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_NETWORK
		*
		*/
		.Const("BASS_DEVICE_TYPE_NETWORK", BASS_DEVICE_TYPE_NETWORK)
		/* squirreldoc (const)
		*
		* Represents a set of speakers.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_SPEAKERS
		*
		*/
		.Const("BASS_DEVICE_TYPE_SPEAKERS", BASS_DEVICE_TYPE_SPEAKERS)
		/* squirreldoc (const)
		*
		* Represents an audio endpoint device that sends a line-level analog signal to a line-input jack on an audio adapter or that receives a line-level analog signal from a line-output jack on the adapter.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_LINE
		*
		*/
		.Const("BASS_DEVICE_TYPE_LINE", BASS_DEVICE_TYPE_LINE)
		/* squirreldoc (const)
		*
		* Represents a set of headphones.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_HEADPHONES
		*
		*/
		.Const("BASS_DEVICE_TYPE_HEADPHONES", BASS_DEVICE_TYPE_HEADPHONES)
		/* squirreldoc (const)
		*
		* Represents a microphone.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_MICROPHONE
		*
		*/
		.Const("BASS_DEVICE_TYPE_MICROPHONE", BASS_DEVICE_TYPE_MICROPHONE)
		/* squirreldoc (const)
		*
		* Represents an earphone or a pair of earphones with an attached mouthpiece for two-way communication.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_HEADSET
		*
		*/
		.Const("BASS_DEVICE_TYPE_HEADSET", BASS_DEVICE_TYPE_HEADSET)
		/* squirreldoc (const)
		*
		* Represents the part of a telephone that is held in the hand and that contains a speaker and a microphone for two-way communication.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_HANDSET
		*
		*/
		.Const("BASS_DEVICE_TYPE_HANDSET", BASS_DEVICE_TYPE_HANDSET)
		/* squirreldoc (const)
		*
		* Represents an audio endpoint device that connects to an audio adapter through a connector for a digital interface of unknown type.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_DIGITAL
		*
		*/
		.Const("BASS_DEVICE_TYPE_DIGITAL", BASS_DEVICE_TYPE_DIGITAL)
		/* squirreldoc (const)
		*
		* Represents an audio endpoint device that connects to an audio adapter through a Sony/Philips Digital Interface (S/PDIF) connector.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_SPDIF
		*
		*/
		.Const("BASS_DEVICE_TYPE_SPDIF", BASS_DEVICE_TYPE_SPDIF)
		/* squirreldoc (const)
		*
		* Represents an audio endpoint device that connects to an audio adapter through a High-Definition Multimedia Interface (HDMI) connector.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_HDMI
		*
		*/
		.Const("BASS_DEVICE_TYPE_HDMI", BASS_DEVICE_TYPE_HDMI)
		/* squirreldoc (const)
		*
		* Represents an audio endpoint device that connects to an audio adapter through a DisplayPort connector.
		*
		* @category Device type
		* @side		client
		* @name		BASS_DEVICE_TYPE_DISPLAYPORT
		*
		*/
		.Const("BASS_DEVICE_TYPE_DISPLAYPORT", BASS_DEVICE_TYPE_DISPLAYPORT)

		/* squirreldoc (const)
		*
		* Represents device supporting all sample rates between the min and maxrate values.
		*
		* @category Device capabilities
		* @side		client
		* @name		DSCAPS_CONTINUOUSRATE
		*
		*/
		.Const("DSCAPS_CONTINUOUSRATE", DSCAPS_CONTINUOUSRATE)
		/* squirreldoc (const)
		*
		* Represents that device does not have a DirectSound driver installed, so it is being emulated through the waveform-audio functions.
		*
		* @category Device capabilities
		* @side		client
		* @name		DSCAPS_EMULDRIVER
		*
		*/
		.Const("DSCAPS_EMULDRIVER", DSCAPS_EMULDRIVER)
		/* squirreldoc (const)
		*
		* Represents that driver has been tested and certified by Microsoft.
		*
		* @category Device capabilities
		* @side		client
		* @name		DSCAPS_CERTIFIED
		*
		*/
		.Const("DSCAPS_CERTIFIED", DSCAPS_CERTIFIED)
		/* squirreldoc (const)
		*
		* Represents that device supports hardware-mixed monophonic secondary buffers.
		*
		* @category Device capabilities
		* @side		client
		* @name		DSCAPS_SECONDARYMONO
		*
		*/
		.Const("DSCAPS_SECONDARYMONO", DSCAPS_SECONDARYMONO)
		/* squirreldoc (const)
		*
		* Represents that device supports hardware-mixed stereo secondary buffers.
		*
		* @category Device capabilities
		* @side		client
		* @name		DSCAPS_SECONDARYSTEREO
		*
		*/
		.Const("DSCAPS_SECONDARYSTEREO", DSCAPS_SECONDARYSTEREO)
		/* squirreldoc (const)
		*
		* Represents that device supports hardware-mixed secondary buffers with 8-bit samples.
		*
		* @category Device capabilities
		* @side		client
		* @name		DSCAPS_SECONDARY8BIT
		*
		*/
		.Const("DSCAPS_SECONDARY8BIT", DSCAPS_SECONDARY8BIT)
		/* squirreldoc (const)
		*
		* Represents that device supports hardware-mixed secondary sound buffers with 16-bit samples.
		*
		* @category Device capabilities
		* @side		client
		* @name		DSCAPS_SECONDARY16BIT
		*
		*/
		.Const("DSCAPS_SECONDARY16BIT", DSCAPS_SECONDARY16BIT)

		/* squirreldoc (const)
		*
		* Represents using 8-bit resolution on by the sample.
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_8BITS
		*
		*/
		.Const("BASS_SAMPLE_8BITS", BASS_SAMPLE_8BITS)
		/* squirreldoc (const)
		*
		* Represents using 32-bit floating-point data by the sample.
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_FLOAT
		*
		*/
		.Const("BASS_SAMPLE_FLOAT", BASS_SAMPLE_FLOAT)
		/* squirreldoc (const)
		*
		* Represents using mono by the sample.
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_MONO
		*
		*/
		.Const("BASS_SAMPLE_MONO", BASS_SAMPLE_MONO)
		/* squirreldoc (const)
		*
		* Represents sample looping (after reaching end, sample will be played again).
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_LOOP
		*
		*/
		.Const("BASS_SAMPLE_LOOP", BASS_SAMPLE_LOOP)
		/* squirreldoc (const)
		*
		* Represents sample 3d functionallity.
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_3D
		*
		*/
		.Const("BASS_SAMPLE_3D", BASS_SAMPLE_3D)
		/* squirreldoc (const)
		*
		* Represents using software mixer instead of hardware one.
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_SOFTWARE
		*
		*/
		.Const("BASS_SAMPLE_SOFTWARE", BASS_SAMPLE_SOFTWARE)
		/* squirreldoc (const)
		*
		* Represents muting sample at at max distance (3d only).
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_MUTEMAX
		*
		*/
		.Const("BASS_SAMPLE_MUTEMAX", BASS_SAMPLE_MUTEMAX)
		/* squirreldoc (const)
		*
		* Represents using DX7 voice allocation & management.
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_VAM
		*
		*/
		.Const("BASS_SAMPLE_VAM", BASS_SAMPLE_VAM)
		/* squirreldoc (const)
		*
		* Represents old implementation of DX8 effects.
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_FX
		*
		*/
		.Const("BASS_SAMPLE_FX", BASS_SAMPLE_FX)
		/* squirreldoc (const)
		*
		* Represents overriding channel with lowest volume.
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_OVER_VOL
		*
		*/
		.Const("BASS_SAMPLE_OVER_VOL", BASS_SAMPLE_OVER_VOL)
		/* squirreldoc (const)
		*
		* Represents overriding the longest playing channel.
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_OVER_POS
		*
		*/
		.Const("BASS_SAMPLE_OVER_POS", BASS_SAMPLE_OVER_POS)
		/* squirreldoc (const)
		*
		* Represents overriding the channel furthest away (from the listener) (3D only).
		*
		* @category Sample
		* @side		client
		* @name		BASS_SAMPLE_OVER_DIST
		*
		*/
		.Const("BASS_SAMPLE_OVER_DIST", BASS_SAMPLE_OVER_DIST)

		/* squirreldoc (const)
		*
		* Represents pre-scanning the file for accurate seek points and length reading in MP3/MP2/MP1 files and chained OGG files.
		*
		* @category Stream
		* @side		client
		* @name		BASS_STREAM_PRESCAN
		*
		*/
		.Const("BASS_STREAM_PRESCAN", BASS_STREAM_PRESCAN)
		/* squirreldoc (const)
		*
		* Alias for `BASS_STREAM_PRESCAN`.
		*
		* @category Stream
		* @side		client
		* @name		BASS_MP3_SETPOS
		*
		*/
		.Const("BASS_MP3_SETPOS", BASS_MP3_SETPOS)
		/* squirreldoc (const)
		*
		* Represents automatically freeing the stream when it stop/ends.
		*
		* @category Stream
		* @side		client
		* @name		BASS_STREAM_AUTOFREE
		*
		*/
		.Const("BASS_STREAM_AUTOFREE", BASS_STREAM_AUTOFREE)
		/* squirreldoc (const)
		*
		* Represents restricting the download rate of internet file streams.
		*
		* @category Stream
		* @side		client
		* @name		BASS_STREAM_RESTRATE
		*
		*/
		.Const("BASS_STREAM_RESTRATE", BASS_STREAM_RESTRATE)
		/* squirreldoc (const)
		*
		* Represents downloading/playing internet file stream in small blocks.
		*
		* @category Stream
		* @side		client
		* @name		BASS_STREAM_BLOCK
		*
		*/
		.Const("BASS_STREAM_BLOCK", BASS_STREAM_BLOCK)
		/* squirreldoc (const)
		*
		* Represents only decoding stream, not playing it.
		*
		* @category Stream
		* @side		client
		* @name		BASS_STREAM_DECODE
		*
		*/
		.Const("BASS_STREAM_DECODE", BASS_STREAM_DECODE)
		/* squirreldoc (const)
		*
		* Represents giving server status info (HTTP/ICY tags) in DOWNLOADPROC.
		*
		* @category Stream
		* @side		client
		* @name		BASS_STREAM_STATUS
		*
		*/
		.Const("BASS_STREAM_STATUS", BASS_STREAM_STATUS)

		/* squirreldoc (const)
		*
		* Represents using 32-bit floating-point data by the MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_SAMPLE_FLOAT
		*
		*/
		.Const("BASS_MUSIC_FLOAT", BASS_MUSIC_FLOAT)
		/* squirreldoc (const)
		*
		* Represents using mono by the MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_SAMPLE_MONO
		*
		*/
		.Const("BASS_MUSIC_MONO", BASS_MUSIC_MONO)
		/* squirreldoc (const)
		*
		* Represents MOD music looping (after reaching end, sample will be played again).
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_LOOP
		*
		*/
		.Const("BASS_MUSIC_LOOP", BASS_MUSIC_LOOP)
		/* squirreldoc (const)
		*
		* Represents MOD music 3d functionallity.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_3D
		*
		*/
		.Const("BASS_MUSIC_3D", BASS_MUSIC_3D)
		/* squirreldoc (const)
		*
		* Represents old implementation of DX8 effects.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_FX
		*
		*/
		.Const("BASS_MUSIC_FX", BASS_MUSIC_FX)
		/* squirreldoc (const)
		*
		* Represents automatically freeing the music MOD when it stop/ends.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_AUTOFREE
		*
		*/
		.Const("BASS_MUSIC_AUTOFREE", BASS_MUSIC_AUTOFREE)
		/* squirreldoc (const)
		*
		* Represents only decoding music MOD, not playing it..
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_DECODE
		*
		*/
		.Const("BASS_MUSIC_DECODE", BASS_MUSIC_DECODE)
		/* squirreldoc (const)
		*
		* Represents pre-scanning the file for accurate seek points and length reading in MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_STREAM_PRESCAN
		*
		*/
		.Const("BASS_MUSIC_PRESCAN", BASS_MUSIC_PRESCAN)
		/* squirreldoc (const)
		*
		* Alias for `BASS_MUSIC_PRESCAN`.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_CALCLEN
		*
		*/
		.Const("BASS_MUSIC_CALCLEN", BASS_MUSIC_CALCLEN)
		/* squirreldoc (const)
		*
		* Represents using "normal" ramping in MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_RAMP
		*
		*/
		.Const("BASS_MUSIC_RAMP", BASS_MUSIC_RAMP)
		/* squirreldoc (const)
		*
		* Represents using "sensitive" ramping in MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_RAMPS
		*
		*/
		.Const("BASS_MUSIC_RAMPS", BASS_MUSIC_RAMPS)
		/* squirreldoc (const)
		*
		* Represents using surround sound in MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_SURROUND
		*
		*/
		.Const("BASS_MUSIC_SURROUND", BASS_MUSIC_SURROUND)
		/* squirreldoc (const)
		*
		* Represents using surround sound mode 2 in MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_SURROUND2
		*
		*/
		.Const("BASS_MUSIC_SURROUND2", BASS_MUSIC_SURROUND2)
		/* squirreldoc (const)
		*
		* Represents playing .MOD as FastTracker 2 does.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_FT2MOD
		*
		*/
		.Const("BASS_MUSIC_FT2MOD", BASS_MUSIC_FT2MOD)
		/* squirreldoc (const)
		*
		* Represents playing .MOD as ProTracker 1 does.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_PT1MOD
		*
		*/
		.Const("BASS_MUSIC_PT1MOD", BASS_MUSIC_PT1MOD)
		/* squirreldoc (const)
		*
		* Represents using non-interpolated sample mixing by MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_NONINTER
		*
		*/
		.Const("BASS_MUSIC_NONINTER", BASS_MUSIC_NONINTER)
		/* squirreldoc (const)
		*
		* Represents using sinc-interpolated sample mixing by MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_SINCINTER
		*
		*/
		.Const("BASS_MUSIC_SINCINTER", BASS_MUSIC_SINCINTER)
		/* squirreldoc (const)
		*
		* Represents stopping all notes when moving position on MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_POSRESET
		*
		*/
		.Const("BASS_MUSIC_POSRESET", BASS_MUSIC_POSRESET)
		/* squirreldoc (const)
		*
		* Represents stopping all notes and reset bmp/etc when moving position on MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_POSRESETEX
		*
		*/
		.Const("BASS_MUSIC_POSRESETEX", BASS_MUSIC_POSRESETEX)
		/* squirreldoc (const)
		*
		* Represents stop the MOD music on a backwards jump effect.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_STOPBACK
		*
		*/
		.Const("BASS_MUSIC_STOPBACK", BASS_MUSIC_STOPBACK)
		/* squirreldoc (const)
		*
		* Represents not loading samples by MOD music.
		*
		* @category Music
		* @side		client
		* @name		BASS_MUSIC_NOSAMPLE
		*
		*/
		.Const("BASS_MUSIC_NOSAMPLE", BASS_MUSIC_NOSAMPLE)

		/* squirreldoc (const)
		*
		* Represents the stereo front speakers.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_FRONT
		*
		*/
		.Const("BASS_SPEAKER_FRONT", BASS_SPEAKER_FRONT)
		/* squirreldoc (const)
		*
		* Represents the stereo rear/side speakers.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_REAR
		*
		*/
		.Const("BASS_SPEAKER_REAR", BASS_SPEAKER_REAR)
		/* squirreldoc (const)
		*
		* Represents the stereo center and LFE (subwoofer) speakers in a 5.1 setup.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_CENLFE
		*
		*/
		.Const("BASS_SPEAKER_CENLFE", BASS_SPEAKER_CENLFE)
		/* squirreldoc (const)
		*
		* Represents the stereo rear center speakers in a 7.1 setup.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_REAR2
		*
		*/
		.Const("BASS_SPEAKER_REAR2", BASS_SPEAKER_REAR2)
		/* squirreldoc (const)
		*
		* Represents the mono left speaker.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_REAR2
		*
		*/
		.Const("BASS_SPEAKER_LEFT", BASS_SPEAKER_LEFT)
		/* squirreldoc (const)
		*
		* Represents the mono right speaker.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_REAR2
		*
		*/
		.Const("BASS_SPEAKER_RIGHT", BASS_SPEAKER_RIGHT)
		/* squirreldoc (const)
		*
		* Represents the front left speaker.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_FRONTLEFT
		*
		*/
		.Const("BASS_SPEAKER_FRONTLEFT", BASS_SPEAKER_FRONTLEFT)
		/* squirreldoc (const)
		*
		* Represents the front right speaker.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_FRONTRIGHT
		*
		*/
		.Const("BASS_SPEAKER_FRONTRIGHT", BASS_SPEAKER_FRONTRIGHT)
		/* squirreldoc (const)
		*
		* Represents the rear left speaker.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_REARLEFT
		*
		*/
		.Const("BASS_SPEAKER_REARLEFT", BASS_SPEAKER_REARLEFT)
		/* squirreldoc (const)
		*
		* Represents the rear right speaker.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_REARRIGHT
		*
		*/
		.Const("BASS_SPEAKER_REARRIGHT", BASS_SPEAKER_REARRIGHT)
		/* squirreldoc (const)
		*
		* Represents the center speaker in a 5.1 speaker setup.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_CENTER
		*
		*/
		.Const("BASS_SPEAKER_CENTER", BASS_SPEAKER_CENTER)
		/* squirreldoc (const)
		*
		* Represents the LFE (subwoofer) speaker in a 5.1 setup.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_CENTER
		*
		*/
		.Const("BASS_SPEAKER_LFE", BASS_SPEAKER_LFE)
		/* squirreldoc (const)
		*
		* Represents the left-rear center speaker in a 7.1 setup.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_REAR2LEFT
		*
		*/
		.Const("BASS_SPEAKER_REAR2LEFT", BASS_SPEAKER_REAR2LEFT)
		/* squirreldoc (const)
		*
		* Represents the right-rear center speaker in a 7.1 setup.
		*
		* @category Speaker
		* @side		client
		* @name		BASS_SPEAKER_REAR2LEFT
		*
		*/
		.Const("BASS_SPEAKER_REAR2RIGHT", BASS_SPEAKER_REAR2RIGHT)

		/* squirreldoc (const)
		*
		* Represents reading the file asynchronously.
		*
		* @category General
		* @side		client
		* @name		BASS_ASYNCFILE
		*
		*/
		.Const("BASS_ASYNCFILE", BASS_ASYNCFILE)
		/* squirreldoc (const)
		*
		* Represents using UTF-16 on a file.
		*
		* @category General
		* @side		client
		* @name		BASS_UNICODE
		*
		*/
		.Const("BASS_UNICODE", static_cast<SQInteger>(BASS_UNICODE))

		/* squirreldoc (const)
		*
		* Represents playing the sample in hardware.
		*
		* @category VAM
		* @side		client
		* @name		BASS_VAM_HARDWARE
		*
		*/
		.Const("BASS_VAM_HARDWARE", BASS_VAM_HARDWARE)
		/* squirreldoc (const)
		*
		* Represents playing the sample in software.
		*
		* @category VAM
		* @side		client
		* @name		BASS_VAM_SOFTWARE
		*
		*/
		.Const("BASS_VAM_SOFTWARE", BASS_VAM_SOFTWARE)
		/* squirreldoc (const)
		*
		* Represents if there are no free hardware voices, the buffer to be terminated will be the one with the least time left to play.
		*
		* @category VAM
		* @side		client
		* @name		BASS_VAM_TERM_TIME
		*
		*/
		.Const("BASS_VAM_TERM_TIME", BASS_VAM_TERM_TIME)
		/* squirreldoc (const)
		*
		* Represents if there are no free hardware voices, the buffer to be terminated will be one that was loaded/created with the `BASS_SAMPLE_MUTEMAX` flag and is beyond its max distance (maxdist).
		*
		* @category VAM
		* @side		client
		* @name		BASS_VAM_TERM_DIST
		*
		*/
		.Const("BASS_VAM_TERM_DIST", BASS_VAM_TERM_DIST)
		/* squirreldoc (const)
		*
		* Represents if there are no free hardware voices, the buffer to be terminated will be the one with the lowest priority.
		*
		* @category VAM
		* @side		client
		* @name		BASS_VAM_TERM_PRIO
		*
		*/
		.Const("BASS_VAM_TERM_PRIO", BASS_VAM_TERM_PRIO)

		/* squirreldoc (const)
		*
		* Represents sample channel type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_SAMPLE
		*
		*/
		.Const("BASS_CTYPE_SAMPLE", BASS_CTYPE_SAMPLE)
		/* squirreldoc (const)
		*
		* Represents stream type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_STREAM
		*
		*/
		.Const("BASS_CTYPE_STREAM", BASS_CTYPE_STREAM)
		/* squirreldoc (const)
		*
		* Represents ogg stream type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_STREAM_OGG
		*
		*/
		.Const("BASS_CTYPE_STREAM_OGG", BASS_CTYPE_STREAM_OGG)
		/* squirreldoc (const)
		*
		* Represents mp1 stream type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_STREAM_MP1
		*
		*/
		.Const("BASS_CTYPE_STREAM_MP1", BASS_CTYPE_STREAM_MP1)
		/* squirreldoc (const)
		*
		* Represents mp2 stream type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_STREAM_MP2
		*
		*/
		.Const("BASS_CTYPE_STREAM_MP2", BASS_CTYPE_STREAM_MP2)
		/* squirreldoc (const)
		*
		* Represents mp3 stream type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_STREAM_MP3
		*
		*/
		.Const("BASS_CTYPE_STREAM_MP3", BASS_CTYPE_STREAM_MP3)
		/* squirreldoc (const)
		*
		* Represents aiff stream type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_STREAM_AIFF
		*
		*/
		.Const("BASS_CTYPE_STREAM_AIFF", BASS_CTYPE_STREAM_AIFF)
		/* squirreldoc (const)
		*
		* Represents CoreAudio codec stream type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_STREAM_CA
		*
		*/
		.Const("BASS_CTYPE_STREAM_CA", BASS_CTYPE_STREAM_CA)
		/* squirreldoc (const)
		*
		* Represents Media Foundation codec stream type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_STREAM_MF
		*
		*/
		.Const("BASS_CTYPE_STREAM_MF", BASS_CTYPE_STREAM_MF)
		/* squirreldoc (const)
		*
		* Represents WAVE format stream type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_STREAM_WAV
		*
		*/
		.Const("BASS_CTYPE_STREAM_WAV", BASS_CTYPE_STREAM_WAV)
		/* squirreldoc (const)
		*
		* Represents integer PCM WAVE format stream type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_STREAM_WAV_PCM
		*
		*/
		.Const("BASS_CTYPE_STREAM_WAV_PCM", BASS_CTYPE_STREAM_WAV_PCM)
		/* squirreldoc (const)
		*
		* Represents floating-point PCM WAVE format stream type.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_STREAM_WAV_FLOAT
		*
		*/
		.Const("BASS_CTYPE_STREAM_WAV_FLOAT", BASS_CTYPE_STREAM_WAV_FLOAT)
		/* squirreldoc (const)
		*
		* Represents generic MOD format music.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_MUSIC_MOD
		*
		*/
		.Const("BASS_CTYPE_MUSIC_MOD", BASS_CTYPE_MUSIC_MOD)
		/* squirreldoc (const)
		*
		* Represents MultiTracker format music.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_MUSIC_MTM
		*
		*/
		.Const("BASS_CTYPE_MUSIC_MTM", BASS_CTYPE_MUSIC_MTM)
		/* squirreldoc (const)
		*
		* Represents ScreamTracker 3 format music.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_MUSIC_S3M
		*
		*/
		.Const("BASS_CTYPE_MUSIC_S3M", BASS_CTYPE_MUSIC_S3M)
		/* squirreldoc (const)
		*
		* Represents FastTracker 2 format music.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_MUSIC_XM
		*
		*/
		.Const("BASS_CTYPE_MUSIC_XM", BASS_CTYPE_MUSIC_XM)
		/* squirreldoc (const)
		*
		* Represents Impulse Tracker format music.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_MUSIC_IT
		*
		*/
		.Const("BASS_CTYPE_MUSIC_IT", BASS_CTYPE_MUSIC_IT)
		/* squirreldoc (const)
		*
		* Represents MO3 format flag.
		*
		* @category Channel type
		* @side		client
		* @name		BASS_CTYPE_MUSIC_MO3
		*
		*/
		.Const("BASS_CTYPE_MUSIC_MO3", BASS_CTYPE_MUSIC_MO3)

		/* squirreldoc (const)
		*
		* Represents normal 3D processing.
		*
		* @category 3d mode
		* @side		client
		* @name		BASS_3DMODE_NORMAL
		*
		*/
		.Const("BASS_3DMODE_NORMAL", BASS_3DMODE_NORMAL)
		/* squirreldoc (const)
		*
		* Represents that the channel's 3D position (position/velocity/orientation) is relative to the listener.
		*
		* @category 3d mode
		* @side		client
		* @name		BASS_3DMODE_RELATIVE
		*
		*/
		.Const("BASS_3DMODE_RELATIVE", BASS_3DMODE_RELATIVE)
		/* squirreldoc (const)
		*
		* Represents that the 3D processing is off.
		*
		* @category 3d mode
		* @side		client
		* @name		BASS_3DMODE_OFF
		*
		*/
		.Const("BASS_3DMODE_OFF", BASS_3DMODE_OFF)

		/* squirreldoc (const)
		*
		* Represents the default 3d algorithm.
		*
		* @category 3d mode
		* @side		client
		* @name		BASS_3DALG_DEFAULT
		*
		*/
		.Const("BASS_3DALG_DEFAULT", BASS_3DALG_DEFAULT)
		/* squirreldoc (const)
		*
		* Represents normal left and right panning.
		*
		* @category 3d algorithm
		* @side		client
		* @name		BASS_3DALG_OFF
		*
		*/
		.Const("BASS_3DALG_OFF", BASS_3DALG_OFF)
		/* squirreldoc (const)
		*
		* Represents algorithm that gives the highest quality 3D audio effect, but uses more CPU.
		*
		* @category 3d algorithm
		* @side		client
		* @name		BASS_3DALG_FULL
		*
		*/
		.Const("BASS_3DALG_FULL", BASS_3DALG_FULL)
		/* squirreldoc (const)
		*
		* Represents algorithm that gives a good 3D audio effect, and uses less CPU than the FULL algorithm.
		*
		* @category 3d algorithm
		* @side		client
		* @name		BASS_3DALG_LIGHT
		*
		*/
		.Const("BASS_3DALG_LIGHT", BASS_3DALG_LIGHT)

		/* squirreldoc (const)
		*
		* Represents generic enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_GENERIC
		*
		*/
		.Const("EAX_ENVIRONMENT_GENERIC", EAX_ENVIRONMENT_GENERIC)
		/* squirreldoc (const)
		*
		* Represents padded cell enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_PADDEDCELL
		*
		*/
		.Const("EAX_ENVIRONMENT_PADDEDCELL", EAX_ENVIRONMENT_PADDEDCELL)
		/* squirreldoc (const)
		*
		* Represents room enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_ROOM
		*
		*/
		.Const("EAX_ENVIRONMENT_ROOM", EAX_ENVIRONMENT_ROOM)
		/* squirreldoc (const)
		*
		* Represents bathroom enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_BATHROOM
		*
		*/
		.Const("EAX_ENVIRONMENT_BATHROOM", EAX_ENVIRONMENT_BATHROOM)
		/* squirreldoc (const)
		*
		* Represents living room enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_LIVINGROOM
		*
		*/
		.Const("EAX_ENVIRONMENT_LIVINGROOM", EAX_ENVIRONMENT_LIVINGROOM)
		/* squirreldoc (const)
		*
		* Represents stone room enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_STONEROOM
		*
		*/
		.Const("EAX_ENVIRONMENT_STONEROOM", EAX_ENVIRONMENT_STONEROOM)
		/* squirreldoc (const)
		*
		* Represents auditorium enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_AUDITORIUM
		*
		*/
		.Const("EAX_ENVIRONMENT_AUDITORIUM", EAX_ENVIRONMENT_AUDITORIUM)
		/* squirreldoc (const)
		*
		* Represents concert hall enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_CONCERTHALL
		*
		*/
		.Const("EAX_ENVIRONMENT_CONCERTHALL", EAX_ENVIRONMENT_CONCERTHALL)
		/* squirreldoc (const)
		*
		* Represents cave enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_CAVE
		*
		*/
		.Const("EAX_ENVIRONMENT_CAVE", EAX_ENVIRONMENT_CAVE)
		/* squirreldoc (const)
		*
		* Represents arena enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_ARENA
		*
		*/
		.Const("EAX_ENVIRONMENT_ARENA", EAX_ENVIRONMENT_ARENA)
		/* squirreldoc (const)
		*
		* Represents hangar enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_HANGAR
		*
		*/
		.Const("EAX_ENVIRONMENT_HANGAR", EAX_ENVIRONMENT_HANGAR)
		/* squirreldoc (const)
		*
		* Represents carpeted hallway enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_CARPETEDHALLWAY
		*
		*/
		.Const("EAX_ENVIRONMENT_CARPETEDHALLWAY", EAX_ENVIRONMENT_CARPETEDHALLWAY)
		/* squirreldoc (const)
		*
		* Represents hallway enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_HALLWAY
		*
		*/
		.Const("EAX_ENVIRONMENT_HALLWAY", EAX_ENVIRONMENT_HALLWAY)
		/* squirreldoc (const)
		*
		* Represents stone corridor enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_STONECORRIDOR
		*
		*/
		.Const("EAX_ENVIRONMENT_STONECORRIDOR", EAX_ENVIRONMENT_STONECORRIDOR)
		/* squirreldoc (const)
		*
		* Represents alley enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_ALLEY
		*
		*/
		.Const("EAX_ENVIRONMENT_ALLEY", EAX_ENVIRONMENT_ALLEY)
		/* squirreldoc (const)
		*
		* Represents forest enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_FOREST
		*
		*/
		.Const("EAX_ENVIRONMENT_FOREST", EAX_ENVIRONMENT_FOREST)
		/* squirreldoc (const)
		*
		* Represents city enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_CITY
		*
		*/
		.Const("EAX_ENVIRONMENT_CITY", EAX_ENVIRONMENT_CITY)
		/* squirreldoc (const)
		*
		* Represents mountains enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_MOUNTAINS
		*
		*/
		.Const("EAX_ENVIRONMENT_MOUNTAINS", EAX_ENVIRONMENT_MOUNTAINS)
		/* squirreldoc (const)
		*
		* Represents quarry enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_QUARRY
		*
		*/
		.Const("EAX_ENVIRONMENT_QUARRY", EAX_ENVIRONMENT_QUARRY)
		/* squirreldoc (const)
		*
		* Represents plain enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_PLAIN
		*
		*/
		.Const("EAX_ENVIRONMENT_PLAIN", EAX_ENVIRONMENT_PLAIN)
		/* squirreldoc (const)
		*
		* Represents parking lot enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_PARKINGLOT
		*
		*/
		.Const("EAX_ENVIRONMENT_PARKINGLOT", EAX_ENVIRONMENT_PARKINGLOT)
		/* squirreldoc (const)
		*
		* Represents sewer pipe enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_SEWERPIPE
		*
		*/
		.Const("EAX_ENVIRONMENT_SEWERPIPE", EAX_ENVIRONMENT_SEWERPIPE)
		/* squirreldoc (const)
		*
		* Represents underwater enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_UNDERWATER
		*
		*/
		.Const("EAX_ENVIRONMENT_UNDERWATER", EAX_ENVIRONMENT_UNDERWATER)
		/* squirreldoc (const)
		*
		* Represents drugged enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_DRUGGED
		*
		*/
		.Const("EAX_ENVIRONMENT_DRUGGED", EAX_ENVIRONMENT_DRUGGED)
		/* squirreldoc (const)
		*
		* Represents dizzy enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_DIZZY
		*
		*/
		.Const("EAX_ENVIRONMENT_DIZZY", EAX_ENVIRONMENT_DIZZY)
		/* squirreldoc (const)
		*
		* Represents psychotic enviormental effect.
		*
		* @category EAX
		* @side		client
		* @name		EAX_ENVIRONMENT_PSYCHOTIC
		*
		*/
		.Const("EAX_ENVIRONMENT_PSYCHOTIC", EAX_ENVIRONMENT_PSYCHOTIC)

		/* squirreldoc (const)
		*
		* Represents unbuffered streamfile.
		*
		* @category StreamFile
		* @side		client
		* @name		STREAMFILE_NOBUFFER
		*
		*/
		.Const("STREAMFILE_NOBUFFER", STREAMFILE_NOBUFFER)
		/* squirreldoc (const)
		*
		* Represents buffered streamfile.
		*
		* @category StreamFile
		* @side		client
		* @name		STREAMFILE_BUFFER
		*
		*/
		.Const("STREAMFILE_BUFFER", STREAMFILE_BUFFER)
		/* squirreldoc (const)
		*
		* Represents user managed bufferred streamfile.
		*
		* @category StreamFile
		* @side		client
		* @name		STREAMFILE_BUFFERPUSH
		*
		*/
		.Const("STREAMFILE_BUFFERPUSH", STREAMFILE_BUFFERPUSH)

		/* squirreldoc (const)
		*
		* Represents position that is to be decoded for playback next.
		*
		* @category File position
		* @side		client
		* @name		BASS_FILEPOS_CURRENT
		*
		*/
		.Const("BASS_FILEPOS_CURRENT", BASS_FILEPOS_CURRENT)
		/* squirreldoc (const)
		*
		* Represents decoding position.
		*
		* @category File position
		* @side		client
		* @name		BASS_FILEPOS_DECODE
		*
		*/
		.Const("BASS_FILEPOS_DECODE", BASS_FILEPOS_DECODE)
		/* squirreldoc (const)
		*
		* Represents download progress of an internet file stream or "buffered" user file stream.
		*
		* @category File position
		* @side		client
		* @name		BASS_FILEPOS_DECODE
		*
		*/
		.Const("BASS_FILEPOS_DOWNLOAD", BASS_FILEPOS_DOWNLOAD)
		/* squirreldoc (const)
		*
		* Represents end of audio data.
		*
		* @category File position
		* @side		client
		* @name		BASS_FILEPOS_END
		*
		*/
		.Const("BASS_FILEPOS_END", BASS_FILEPOS_END)
		/* squirreldoc (const)
		*
		* Represents start of audio data.
		*
		* @category File position
		* @side		client
		* @name		BASS_FILEPOS_START
		*
		*/
		.Const("BASS_FILEPOS_START", BASS_FILEPOS_START)
		/* squirreldoc (const)
		*
		* Represents if internet file stream or "buffered" user file stream is still connected.
		*
		* @category File position
		* @side		client
		* @name		BASS_FILEPOS_CONNECTED
		*
		*/
		.Const("BASS_FILEPOS_CONNECTED", BASS_FILEPOS_CONNECTED)
		/* squirreldoc (const)
		*
		* Represents the amount of data in the buffer of an internet file stream or "buffered" user file stream.
		*
		* @category File position
		* @side		client
		* @name		BASS_FILEPOS_BUFFER
		*
		*/
		.Const("BASS_FILEPOS_BUFFER", BASS_FILEPOS_BUFFER)
		/* squirreldoc (const)
		*
		* Represents the socket handle used for streaming.
		*
		* @category File position
		* @side		client
		* @name		BASS_FILEPOS_SOCKET
		*
		*/
		.Const("BASS_FILEPOS_SOCKET", BASS_FILEPOS_SOCKET)
		/* squirreldoc (const)
		*
		* Represents the amount of data in the asynchronous file reading buffer.
		*
		* @category File position
		* @side		client
		* @name		BASS_FILEPOS_ASYNCBUF
		*
		*/
		.Const("BASS_FILEPOS_ASYNCBUF", BASS_FILEPOS_ASYNCBUF)
		/* squirreldoc (const)
		*
		* Represents the total size of the file.
		*
		* @category File position
		* @side		client
		* @name		BASS_FILEPOS_SIZE
		*
		*/
		.Const("BASS_FILEPOS_SIZE", BASS_FILEPOS_SIZE)

		/* squirreldoc (const)
		*
		* Represents the channel active status, or handle is not a valid channel.
		*
		* @category Active
		* @side		client
		* @name		BASS_ACTIVE_STOPPED
		*
		*/
		.Const("BASS_ACTIVE_STOPPED", BASS_ACTIVE_STOPPED)
		/* squirreldoc (const)
		*
		* Represents the channel playing (or recording) status.
		*
		* @category Active
		* @side		client
		* @name		BASS_ACTIVE_PLAYING
		*
		*/
		.Const("BASS_ACTIVE_PLAYING", BASS_ACTIVE_PLAYING)
		/* squirreldoc (const)
		*
		* Represents playback of the stream has been stalled due to a lack of sample data. Playback will automatically resume once there is sufficient data to do so.
		*
		* @category Active
		* @side		client
		* @name		BASS_ACTIVE_STALLED
		*
		*/
		.Const("BASS_ACTIVE_STALLED", BASS_ACTIVE_STALLED)
		/* squirreldoc (const)
		*
		* Represents the channel is paused.
		*
		* @category Active
		* @side		client
		* @name		BASS_ACTIVE_PAUSED
		*
		*/
		.Const("BASS_ACTIVE_PAUSED", BASS_ACTIVE_PAUSED)

		/* squirreldoc (const)
		*
		* Represents sample rate.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_FREQ
		*
		*/
		.Const("BASS_ATTRIB_FREQ", BASS_ATTRIB_FREQ)
		/* squirreldoc (const)
		*
		* Represents volume level.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_VOL
		*
		*/
		.Const("BASS_ATTRIB_VOL", BASS_ATTRIB_VOL)
		/* squirreldoc (const)
		*
		* Represents panning/balance position.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_PAN
		*
		*/
		.Const("BASS_ATTRIB_PAN", BASS_ATTRIB_PAN)
		/* squirreldoc (const)
		*
		* Represents the wet (reverb) / dry (no reverb) mix ratio of a channel.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_EAXMIX
		*
		*/
		.Const("BASS_ATTRIB_EAXMIX", BASS_ATTRIB_EAXMIX)
		/* squirreldoc (const)
		*
		* Represents disabled buffering.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_NOBUFFER
		*
		*/
		.Const("BASS_ATTRIB_NOBUFFER", BASS_ATTRIB_NOBUFFER)
		/* squirreldoc (const)
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_VBR
		*
		*/
		.Const("BASS_ATTRIB_VBR", BASS_ATTRIB_VBR)
		/* squirreldoc (const)
		*
		* Represents CPU usage.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_CPU
		*
		*/
		.Const("BASS_ATTRIB_CPU", BASS_ATTRIB_CPU)
		/* squirreldoc (const)
		*
		* Represents sample rate conversion quality.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_SRC
		*
		*/
		.Const("BASS_ATTRIB_SRC", BASS_ATTRIB_SRC)
		/* squirreldoc (const)
		*
		* Represents buffer level to resume stalled playback.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_NET_RESUME
		*
		*/
		.Const("BASS_ATTRIB_NET_RESUME", BASS_ATTRIB_NET_RESUME)
		/* squirreldoc (const)
		*
		* Represents scanned info.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_SCANINFO
		*
		*/
		.Const("BASS_ATTRIB_SCANINFO", BASS_ATTRIB_SCANINFO)
		/* squirreldoc (const)
		*
		* Represents amplification level.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_MUSIC_AMPLIFY
		*
		*/
		.Const("BASS_ATTRIB_MUSIC_AMPLIFY", BASS_ATTRIB_MUSIC_AMPLIFY)
		/* squirreldoc (const)
		*
		* Represents pan separation level.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_MUSIC_PANSEP
		*
		*/
		.Const("BASS_ATTRIB_MUSIC_PANSEP", BASS_ATTRIB_MUSIC_PANSEP)
		/* squirreldoc (const)
		*
		* Represents position scaler.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_MUSIC_PSCALER
		*
		*/
		.Const("BASS_ATTRIB_MUSIC_PSCALER", BASS_ATTRIB_MUSIC_PSCALER)
		/* squirreldoc (const)
		*
		* Represents BPM.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_MUSIC_PSCALER
		*
		*/
		.Const("BASS_ATTRIB_MUSIC_BPM", BASS_ATTRIB_MUSIC_BPM)
		/* squirreldoc (const)
		*
		* Represents speed.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_MUSIC_PSCALER
		*
		*/
		.Const("BASS_ATTRIB_MUSIC_SPEED", BASS_ATTRIB_MUSIC_SPEED)
		/* squirreldoc (const)
		*
		* Represents global volume level.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_MUSIC_PSCALER
		*
		*/
		.Const("BASS_ATTRIB_MUSIC_VOL_GLOBAL", BASS_ATTRIB_MUSIC_VOL_GLOBAL)
		/* squirreldoc (const)
		*
		* Represents active channel count.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_MUSIC_PSCALER
		*
		*/
		.Const("BASS_ATTRIB_MUSIC_ACTIVE", BASS_ATTRIB_MUSIC_ACTIVE)
		/* squirreldoc (const)
		*
		* Represents a channel volume level.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_MUSIC_VOL_CHAN
		*
		*/
		.Const("BASS_ATTRIB_MUSIC_VOL_CHAN", BASS_ATTRIB_MUSIC_VOL_CHAN)
		/* squirreldoc (const)
		*
		* Represents an instrument/sample volume level.
		*
		* @category Attribute
		* @side		client
		* @name		BASS_ATTRIB_MUSIC_VOL_INST
		*
		*/
		.Const("BASS_ATTRIB_MUSIC_VOL_INST", BASS_ATTRIB_MUSIC_VOL_INST)

		/* squirreldoc (const)
		*
		* Represents mono level. If neither this or the `BASS_LEVEL_STEREO` flag is used then a separate level is retrieved for each channel.
		*
		* @category Level
		* @side		client
		* @name		BASS_LEVEL_MONO
		*
		*/
		.Const("BASS_LEVEL_MONO", BASS_LEVEL_MONO)
		/* squirreldoc (const)
		*
		* Represents stereo level. The left level will be from the even channels, and the right level will be from the odd channels. If there are an odd number of channels then the left and right levels will both include all channels.
		*
		* @category Level
		* @side		client
		* @name		BASS_LEVEL_STEREO
		*
		*/
		.Const("BASS_LEVEL_STEREO", BASS_LEVEL_STEREO)
		/* squirreldoc (const)
		*
		* Represents RMS level. Otherwise the peak level.
		*
		* @category Level
		* @side		client
		* @name		BASS_LEVEL_RMS
		*
		*/
		.Const("BASS_LEVEL_RMS", BASS_LEVEL_RMS)

		/* squirreldoc (const)
		*
		* Represents ID3v1 tags.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_ID3
		*
		*/
		.Const("BASS_TAG_ID3", BASS_TAG_ID3)
		/* squirreldoc (const)
		*
		* Represents ID3v2 tags.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_ID3V2
		*
		*/
		.Const("BASS_TAG_ID3V2", BASS_TAG_ID3V2)
		/* squirreldoc (const)
		*
		* Represents OGG comments.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_OGG
		*
		*/
		.Const("BASS_TAG_OGG", BASS_TAG_OGG)
		/* squirreldoc (const)
		*
		* Represents HTTP headers, only available when streaming from a HTTP server.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_HTTP
		*
		*/
		.Const("BASS_TAG_HTTP", BASS_TAG_HTTP)
		/* squirreldoc (const)
		*
		* Represents ICY (Shoutcast) tags.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_ICY
		*
		*/
		.Const("BASS_TAG_ICY", BASS_TAG_ICY)
		/* squirreldoc (const)
		*
		* Represents Shoutcast metadata. A single string is returned, containing the current stream title and url (usually omitted). The format of the string is: StreamTitle='xxx';StreamUrl='xxx';
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_META
		*
		*/
		.Const("BASS_TAG_META", BASS_TAG_META)
		/* squirreldoc (const)
		*
		* Represents APE (v1 or v2) tags.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_APE
		*
		*/
		.Const("BASS_TAG_APE", BASS_TAG_APE)
		/* squirreldoc (const)
		*
		* Represents MP4/iTunes metadata.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_MP4
		*
		*/
		.Const("BASS_TAG_MP4", BASS_TAG_MP4)
		/* squirreldoc (const)
		*
		* Represents OGG encoder. A single UTF-8 string is returned.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_VENDOR
		*
		*/
		.Const("BASS_TAG_VENDOR", BASS_TAG_VENDOR)
		/* squirreldoc (const)
		*
		* Represents Lyrics3v2 tag. A single string is returned, containing the Lyrics3v2 information. See id3.org/Lyrics3v2 for details of its format.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_LYRICS3
		*
		*/
		.Const("BASS_TAG_LYRICS3", BASS_TAG_LYRICS3)
		/* squirreldoc (const)
		*
		* Represents CoreAudio codec information.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_CA_CODEC
		*
		*/
		.Const("BASS_TAG_CA_CODEC", BASS_TAG_CA_CODEC)
		/* squirreldoc (const)
		*
		* Represents Media Foundation metadata.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_MF
		*
		*/
		.Const("BASS_TAG_MF", BASS_TAG_MF)
		/* squirreldoc (const)
		*
		* Represents WAVE "fmt" chunk contents.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_WAVEFORMAT
		*
		*/
		.Const("BASS_TAG_WAVEFORMAT", BASS_TAG_WAVEFORMAT)
		/* squirreldoc (const)
		*
		* Represents RIFF "INFO" chunk tags.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_RIFF_INFO
		*
		*/
		.Const("BASS_TAG_RIFF_INFO", BASS_TAG_RIFF_INFO)
		/* squirreldoc (const)
		*
		* Represents RIFF/BWF "bext" chunk tags.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_RIFF_BEXT
		*
		*/
		.Const("BASS_TAG_RIFF_BEXT", BASS_TAG_RIFF_BEXT)
		/* squirreldoc (const)
		*
		* Represents RIFF/BWF "cart" chunk tags.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_RIFF_CART
		*
		*/
		.Const("BASS_TAG_RIFF_CART", BASS_TAG_RIFF_CART)
		/* squirreldoc (const)
		*
		* Represents RIFF "DISP" chunk text (CF_TEXT) tag.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_RIFF_DISP
		*
		*/
		.Const("BASS_TAG_RIFF_DISP", BASS_TAG_RIFF_DISP)
		/* squirreldoc (const)
		*
		* Represents APE binary tag.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_APE_BINARY
		*
		*/
		.Const("BASS_TAG_APE_BINARY", BASS_TAG_APE_BINARY)
		/* squirreldoc (const)
		*
		* Represents MOD music title.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_MUSIC_NAME
		*
		*/
		.Const("BASS_TAG_MUSIC_NAME", BASS_TAG_MUSIC_NAME)
		/* squirreldoc (const)
		*
		* Represents MOD message text.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_MUSIC_MESSAGE
		*
		*/
		.Const("BASS_TAG_MUSIC_MESSAGE", BASS_TAG_MUSIC_MESSAGE)
		/* squirreldoc (const)
		*
		* Represents MOD music order list.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_MUSIC_ORDERS
		*
		*/
		.Const("BASS_TAG_MUSIC_ORDERS", BASS_TAG_MUSIC_ORDERS)
		/* squirreldoc (const)
		*
		* Represents MOD instrument name. Only available with formats that have instruments, eg. IT and XM (and MO3).
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_MUSIC_INST
		*
		*/
		.Const("BASS_TAG_MUSIC_INST", BASS_TAG_MUSIC_INST)
		/* squirreldoc (const)
		*
		* Represents MOD sample name.
		*
		* @category Tag
		* @side		client
		* @name		BASS_TAG_MUSIC_SAMPLE
		*
		*/
		.Const("BASS_TAG_MUSIC_SAMPLE", BASS_TAG_MUSIC_SAMPLE)

		/* squirreldoc (const)
		*
		* Represents byte position.
		*
		* @category Position
		* @side		client
		* @name		BASS_POS_BYTE
		*
		*/
		.Const("BASS_POS_BYTE", BASS_POS_BYTE)
		/* squirreldoc (const)
		*
		* Represents position in orders and rows.
		*
		* @category Position
		* @side		client
		* @name		BASS_POS_MUSIC_ORDER
		*
		*/
		.Const("BASS_POS_MUSIC_ORDER", BASS_POS_MUSIC_ORDER)
		/* squirreldoc (const)
		*
		* Represents the number of bitstreams in an OGG file.
		*
		* @category Position
		* @side		client
		* @name		BASS_POS_OGG
		*
		*/
		.Const("BASS_POS_OGG", BASS_POS_OGG)
		/* squirreldoc (const)
		*
		* Represents allowing inexact seeking.
		*
		* @category Position
		* @side		client
		* @name		BASS_POS_INEXACT
		*
		*/
		.Const("BASS_POS_INEXACT", BASS_POS_INEXACT)
		/* squirreldoc (const)
		*
		* Represents decoding position.
		*
		* @category Position
		* @side		client
		* @name		BASS_POS_DECODE
		*
		*/
		.Const("BASS_POS_DECODE", BASS_POS_DECODE)
		/* squirreldoc (const)
		*
		* Represents decoding/rendering up to the position rather than seeking to it.
		*
		* @category Position
		* @side		client
		* @name		BASS_POS_DECODETO
		*
		*/
		.Const("BASS_POS_DECODETO", BASS_POS_DECODETO)
		/* squirreldoc (const)
		*
		* Represents scanning the file to build a seek table up to the position, if it has not already been scanned.
		*
		* @category Position
		* @side		client
		* @name		BASS_POS_SCAN
		*
		*/
		.Const("BASS_POS_SCAN", BASS_POS_SCAN)

		/* squirreldoc (const)
		*
		* Represents DX8 Chorus.
		*
		* @category DX8 Effects
		* @side		client
		* @name		BASS_FX_DX8_CHORUS
		*
		*/
		.Const("BASS_FX_DX8_CHORUS", BASS_FX_DX8_CHORUS)
		/* squirreldoc (const)
		*
		* Represents DX8 Compression.
		*
		* @category DX8 Effects
		* @side		client
		* @name		BASS_FX_DX8_COMPRESSOR
		*
		*/
		.Const("BASS_FX_DX8_COMPRESSOR", BASS_FX_DX8_COMPRESSOR)
		/* squirreldoc (const)
		*
		* Represents DX8 Distortion.
		*
		* @category DX8 Effects
		* @side		client
		* @name		BASS_FX_DX8_DISTORTION
		*
		*/
		.Const("BASS_FX_DX8_DISTORTION", BASS_FX_DX8_DISTORTION)
		/* squirreldoc (const)
		*
		* Represents DX8 Echo.
		*
		* @category DX8 Effects
		* @side		client
		* @name		BASS_FX_DX8_ECHO
		*
		*/
		.Const("BASS_FX_DX8_ECHO", BASS_FX_DX8_ECHO)
		/* squirreldoc (const)
		*
		* Represents DX8 Flanger.
		*
		* @category DX8 Effects
		* @side		client
		* @name		BASS_FX_DX8_FLANGER
		*
		*/
		.Const("BASS_FX_DX8_FLANGER", BASS_FX_DX8_FLANGER)
		/* squirreldoc (const)
		*
		* Represents DX8 Gargle.
		*
		* @category DX8 Effects
		* @side		client
		* @name		BASS_FX_DX8_GARGLE
		*
		*/
		.Const("BASS_FX_DX8_GARGLE", BASS_FX_DX8_GARGLE)
		/* squirreldoc (const)
		*
		* Represents DX8 I3DL2.
		*
		* @category DX8 Effects
		* @side		client
		* @name		BASS_FX_DX8_I3DL2REVERB
		*
		*/
		.Const("BASS_FX_DX8_I3DL2REVERB", BASS_FX_DX8_I3DL2REVERB)
		/* squirreldoc (const)
		*
		* Represents DX8 Parametric equalizer.
		*
		* @category DX8 Effects
		* @side		client
		* @name		BASS_FX_DX8_PARAMEQ
		*
		*/
		.Const("BASS_FX_DX8_PARAMEQ", BASS_FX_DX8_PARAMEQ)
		/* squirreldoc (const)
		*
		* Represents DX8 Reverb.
		*
		* @category DX8 Effects
		* @side		client
		* @name		BASS_FX_DX8_REVERB
		*
		*/
		.Const("BASS_FX_DX8_REVERB", BASS_FX_DX8_REVERB)
	;

	return SQ_OK;
}
