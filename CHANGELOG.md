### Changelog

- Fixed memory leak in `sq_bass_streamcreatefile` (missing `delete` operation in if guards)